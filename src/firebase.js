import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBk-KNfm1noP0ZqL05sHE1EnFC24oHbYF4",
    authDomain: "loggicare.firebaseapp.com",
    projectId: "loggicare",
    storageBucket: "loggicare.appspot.com",
    messagingSenderId: "629874960118",
    appId: "1:629874960118:web:9264ed9bfd79b3a13ceed7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


const auth = firebase.auth()
const db = firebase.firestore()
const storage = firebase.storage()

export { auth, firebase, db, storage }