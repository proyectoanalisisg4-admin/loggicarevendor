import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { actualizarUsuarioAccion, editarFotoAccion, resetPassword, editarFotoPerfil } from '../redux/usuarioDucks'
import { toast, ToastContainer } from 'react-toastify';
import { Form } from 'react-bootstrap';
const Perfil = () => {

    const usuario = useSelector(store => store.usuario.user)
    const loading = useSelector(store => store.usuario.loading)

    const [nombreUsuario, setNombreUsuario] = React.useState(usuario.displayName)
    const [activarFormulario, setActivarFormulario] = React.useState(false)
    const [error, setError] = React.useState(false)

    const dispatch = useDispatch()

    const actualizarUsuario = () => {

        if (!nombreUsuario.trim()) {
            console.log('Nombre vacio')
            return
        }
        dispatch(actualizarUsuarioAccion(nombreUsuario))
        setActivarFormulario(false)
    }

    const seleccionarArchivo = (imagen) => {
        const imagenCliente = imagen.target.files[0]
        console.log(imagenCliente)

        if (imagenCliente === undefined) {
            console.log("Incorrecto")
            return
        }

        if (imagenCliente.type === "image/png" || imagenCliente.type === "image/jpg" || imagenCliente.type === "image/jpeg") {
            console.log("cambiar")
            dispatch(editarFotoPerfil(imagenCliente))
        } else {
            toast.warning('Solo archivos .png o .jpg', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
    }



    return (

        <div class="">

    
            <div class="card card-primary card-outline mt-5">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img className="profile-user-img img-fluid img-circle " src={usuario.photoURL} alt="Foto" width="100px" style={{ borderRadius: "50px" }} />
                    </div>
                    <h3 class="profile-username text-center">{usuario.userName}</h3>
                    <p class="text-muted text-center">{usuario.role}</p>
                    <div className="text-center ">
                        <div className="custom-file mb-3">
                            <input
                                type="file"
                                className="custom-file-input"
                                id="inputGroupFile01"
                                style={{ display: 'none' }}
                                onChange={e => seleccionarArchivo(e)}
                                disabled={loading}
                            />
                            <label className={loading ? 'btn btn-dark mt-2 disabled' : 'btn btn-dark mt-2 '} htmlFor="inputGroupFile01">Actualizar imagen</label>
                        </div>
                        <button type="button" className="btn btn-dark" onClick={() => dispatch(resetPassword(usuario.email))}>Actualizar contraseña</button>

                    </div>

                    <div class=" d-flex justify-content-around mt-5">

                        <div>
                            <strong><i class="fas fa-book "></i> Cédula</strong>
                            <p class="text-muted">
                                {usuario.cedula}
                            </p>
                        </div>

                        <div>
                            <strong><i class="fas fa-phone mr-1"></i> Teléfono</strong>
                            <p class="text-muted"> {usuario.phone}</p>
                        </div>

                        <div>
                            <strong><i class="fas fa-inbox mr-1"></i> Correo</strong>
                            <p class="text-muted">
                                {usuario.email}
                            </p>
                        </div>



                    </div>
                </div>

            </div>
   
            </div>






    )
}

export default Perfil
