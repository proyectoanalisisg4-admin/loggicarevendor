import React from 'react'
import { firebase } from '../../firebase'
import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'
import { Button, Modal, Form } from 'react-bootstrap';
import moment from 'moment'
import 'moment/locale/es' // Pasar a español

//Save pdf
import { Page, Text, View, Document, StyleSheet, Image } from '@react-pdf/renderer';
import { PDFDownloadLink } from '@react-pdf/renderer';


//Factura
import logo from '../../assets/images/logo.png'
import InvoiceTableRow from './InvoiceTableRow'
// Table search and pagination
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'
import Pagination from '../Datatable/Pagination'

const Index = () => {

    const [facturas, setFacturas] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    const [show, setShow] = React.useState(false);
    const [mostrarProductos, setMostrarProductos] = React.useState(false);
    const [datosFactura, setDatosFactura] = React.useState([]);

    const [id, setId] = React.useState()
    const [cedula, setCedula] = React.useState()
    const [nombre, setNombre] = React.useState()
    const [correo, setCorreo] = React.useState()
    const [telefono, setTelefono] = React.useState()
    const [rol, setRol] = React.useState()
    const [foto, setFoto] = React.useState()
    const [userId, setUserId] = React.useState()

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const mostrarMenos = () => setMostrarProductos(false)

    // Table search and pagination
    const ITEMS_PER_PAGE = 5;
    const [totalItems, setTotalItems] = React.useState(0);
    const [currentPage, setCurrentPage] = React.useState();
    const [search, setSearch] = React.useState('')

    // Table search and pagination
    const tableData = React.useMemo(() => {

        let computedTable = facturas;
        if (search) {
            computedTable = computedTable.filter(
                table => table.cedula.toLowerCase().includes(search.toLowerCase()) || table.nombre.toLowerCase().includes(search.toLowerCase())
            )
        }
        setTotalItems(computedTable.length)

        // Current page slice
        return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
    }, [facturas, currentPage, search])

    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {
                const db = firebase.firestore()
                const data = await db.collection('facturas').orderBy('fechaCreacion', 'desc').get()
                const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
                setFacturas(arrayData)
                setLoading(false)
                setCurrentPage(1)

            } catch (error) {
                console.log(error)
            }
        }

        obtenerDatos()
        // eslint-disable-next-line
    }, [])

    const eliminar = async (id) => {
        try {
            const db = firebase.firestore()
            await db.collection('facturas').doc(id).delete()

            const arrayFiltrado = facturas.filter(item => item.id !== id)

            setFacturas(arrayFiltrado)

            toast.success('Factura elimada correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });


        } catch (error) {
            console.log(error)
        }
    }

    const modoEdicion = (item) => {
        setId(item.id)
        setCedula(item.cedula)
        setNombre(item.userName)
        setCorreo(item.email)
        setTelefono(item.phone)
        setRol(item.role)
        setUserId(item.uid)
        setFoto(item.photoURL)
        handleShow()
    }

    const editar = async () => {
        try {

            const db = firebase.firestore()
            await db.collection('usuarios').doc(id).update({
                cedula: cedula,
                userName: nombre,
                email: correo,
                phone: telefono,
                role: rol
            })
            const arrayEditado = facturas.map(item => (
                item.id === id ? { id: item.id, cedula: cedula, userName: nombre, email: correo, phone: telefono, role: rol } : item
            ))

            setFacturas(arrayEditado)
            setId('')
            handleClose()
        } catch (error) {
            console.log(error)
        }
    }

    const verMas = async (item) => {
        setDatosFactura(item)
        setMostrarProductos(true)
    }

    const styles = StyleSheet.create({
        page: {
            fontFamily: 'Helvetica',
            fontSize: 11,
            paddingTop: 30,
            paddingLeft: 60,
            paddingRight: 60,
            lineHeight: 1.5,
            flexDirection: 'column',
        },
        logo: {
            width: 150,
            height: 100,
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        invoiceNoContainer: {
            flexDirection: 'row',
            marginTop: 36,
            justifyContent: 'flex-end'
        },
        invoiceDateContainer: {
            flexDirection: 'row',
            justifyContent: 'flex-end'
        },
        invoiceDate: {
            fontSize: 12,
            fontStyle: 'bold',
        },
        label: {
            width: 60
        },
        headerContainer: {
            marginTop: 36
        },
        billTo: {
            marginTop: 20,
            paddingBottom: 3,
            fontFamily: 'Helvetica-Oblique'
        },
        tableContainer: {
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginTop: 24,
            borderWidth: 1,
            borderColor: '#bff0fd',
        },
        container: {
            flexDirection: 'row',
            borderBottomColor: '#bff0fd',
            backgroundColor: '#bff0fd',
            borderBottomWidth: 1,
            alignItems: 'center',
            height: 24,
            textAlign: 'center',
            fontStyle: 'bold',
            flexGrow: 1,
        },
        id: {
            width: '30%',
            borderRightColor: '#90e5fc',
            borderRightWidth: 1,
        },
        description: {
            width: '30%',
            borderRightColor: '#90e5fc',
            borderRightWidth: 1,
        },
        qty: {
            width: '20%',
            borderRightColor: '#90e5fc',
            borderRightWidth: 1,
        },
        amount: {
            width: '15%'
        },
        rowFooter: {
            flexDirection: 'row',
            borderBottomColor: '#bff0fd',
            borderBottomWidth: 1,
            alignItems: 'center',
            height: 24,
            fontSize: 12,
            fontStyle: 'bold',
        },
        descriptionFooter: {
            width: '85%',
            textAlign: 'right',
            borderRightColor: '#90e5fc',
            borderRightWidth: 1,
            paddingRight: 8,
        },
        total: {
            width: '15%',
            textAlign: 'right',
            paddingRight: 8,
        }

    });

    const MyDocument = ({ invoice }) => (
        <Document>
            <Page size="A4" style={styles.page}>
                <Image style={styles.logo} src={logo} />
                <View style={styles.invoiceNoContainer}>
                    <Text style={styles.label}>Factura:</Text>
                    <Text style={styles.invoiceDate}>{invoice.idFactura}</Text>
                </View >
                <View style={styles.invoiceDateContainer}>
                    <Text style={styles.label}>Fecha: </Text>
                    <Text >{moment(invoice.fechaCreacion).format('MMMM DD YYYY')}</Text>
                </View >
                <View style={styles.headerContainer}>
                    <Text style={styles.billTo}>Facturar a:</Text>
                    <Text>{invoice.nombre}</Text>
                    <Text>{invoice.direccionEnvio}</Text>
                    <Text>{invoice.telefono}</Text>
                    <Text>{invoice.correo}</Text>
                </View>
                <View style={styles.tableContainer}>
                    <View style={styles.container}>
                        <Text style={styles.description}>ID</Text>
                        <Text style={styles.description}>NOMBRE</Text>
                        <Text style={styles.qty}>Cantidad</Text>
                        <Text style={styles.amount}>Precio</Text>
                    </View>
                    <InvoiceTableRow items={invoice.datosFactura} />
                    <View style={styles.rowFooter}>
                        <Text style={styles.descriptionFooter}>TOTAL</Text>
                        <Text style={styles.total}>{invoice.precioTotal}</Text>
                    </View>
                </View>
            </Page>
        </Document>
    );



    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col">
                    <Modal show={show} onHide={handleClose} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Editar Factura</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="container">
                                <div className="row">
                                    <div class="col">
                                        <Form>
                                            <Form.Group className="mb-3" controlId="formGridNombre">
                                                <Form.Label>Cédula</Form.Label>
                                                <Form.Control onChange={e => setCedula(e.target.value)} value={cedula} />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridCodigo">
                                                <Form.Label>Nombre</Form.Label>
                                                <Form.Control onChange={e => setNombre(e.target.value)} value={nombre} type='text' />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridPrecio">
                                                <Form.Label>Correo</Form.Label>
                                                <Form.Control type='email' onChange={e => setCorreo(e.target.value)} value={correo} />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridUnidades">
                                                <Form.Label>Télefono</Form.Label>
                                                <Form.Control type='text' onChange={e => setTelefono(e.target.value)} value={telefono} />
                                            </Form.Group>
                                            <button to="Dashboard" type="button" onClick={() => editar()} className="btn btn-primary mr-3" >Guardar</button>

                                        </Form>


                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={mostrarProductos} onHide={mostrarMenos} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Productos</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="table-responsive">
                                <table className="table mt-4">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Precio</th>
                                            <th scope="col">Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            datosFactura.map((item, index) => (
                                                <tr key={index}>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>₡ {item.price}</td>
                                                    <td>{item.value}</td>
                                                </tr>
                                            ))
                                        }


                                    </tbody>
                                </table>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={mostrarMenos}>
                                Cerrar
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
                <div className="col-12">
                    <div class="clearfix">
                        <div class="pull-left">
                            <div class="c-content-title-1">
                                <h3 class="font-weight-bold">Lista de Facturas</h3>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <Link type="button" to="/Facturar" class="btn btn-success mt-3 mb-3 mr-1"><span class="fa fa-plus"></span> Facturar Ventas</Link>
                            <Link type="button" to="/CrearFactura" class="btn btn-success mt-3 mb-3"><span class="fa fa-plus"></span> Registrar Factura</Link>
                        </div>
                    </div>

                    <Search onSearch={(value) => {
                        setSearch(value)
                        setCurrentPage(1)
                    }
                    }
                    />
                    <table class="table table-hover table-striped tablas">
                        <thead>
                            <tr>
                                <th scope="col">Cédula</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Télefono</th>
                                <th scope="col">Fecha de creación</th>
                                <th scope="col">Acción</th>
                                <th scope="col">Descargar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                tableData.map((item, index) => (
                                    <tr key={index}>
                                        <th>{item.cedula}</th>
                                        <td>{item.nombre}</td>
                                        <td>{item.correo}</td>
                                        <td>{item.telefono}</td>
                                        <td>{moment(item.fechaCreacion).format('MMMM DD YYYY, h:mm:ss a')}</td>
                                        <td class="btn-group d-flex justify-content-center">
                                            <button type="button" class="btn btn-info" onClick={() => verMas(item.datosFactura)}>Ver</button>
                                            <button type="button" class="btn btn-danger" onClick={() => eliminar(item.id)}>Eliminar</button>
                                        </td>
                                        <td>
                                            <PDFDownloadLink document={<MyDocument invoice={item} />} fileName="factura.pdf">
                                                <button type="button" class="btn btn-info">Descargar</button>
                                            </PDFDownloadLink>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    <Pagination
                        total={totalItems}
                        itemsPerPage={ITEMS_PER_PAGE}
                        currentPage={currentPage}
                        onPageChange={page => setCurrentPage(page)}
                    />
                    {loading && <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>}
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    )
}

export default Index