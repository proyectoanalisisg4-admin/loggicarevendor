import React from 'react'


import { useDispatch, useSelector } from 'react-redux'
import { addUser } from '../../redux/vendedorDucks'
import { Link } from 'react-router-dom'

import { withRouter } from 'react-router-dom'

import { toast, ToastContainer } from 'react-toastify';

import InputMask from 'react-input-mask';

import { firebase } from '../../firebase'

//table
import { Modal, Button } from 'react-bootstrap';
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'

//Short id
import shortid from 'shortid';


const Create = (props) => {
    const dispatch = useDispatch()

    const [cedula, setCedula] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [userName, setUserName] = React.useState('')
    const [phone, setPhone] = React.useState('')
    const [envio, setEnvio] = React.useState('No')
    const [direccionEnvio, setDireccionEnvio] = React.useState('')
    const [informacionAdicional, setInformacionAdicional] = React.useState('')

    const [productos, setProductos] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    const [productosFactura, setProductosFactura] = React.useState([])
    const [total, setTotal] = React.useState(0)

    //Table

    const ITEMS_PER_PAGE = 8;
    const [show, setShow] = React.useState(false);

    const handleClose = () => {
        let totalPrice = 0
        // eslint-disable-next-line
        productosFactura.map((item) => {
            totalPrice = totalPrice + item.price
        })
        setTotal(totalPrice)
        setShow(false);
    }

    const handleShow = (type) => {
        setShow(true);
    }
    const [totalItems, setTotalItems] = React.useState(0);
    const [currentPage, setCurrentPage] = React.useState();
    const [search, setSearch] = React.useState('')

    const headers = [{ name: "Código", field: "Codigo" },
    { name: "Nombre", field: "Nombre" },
    { name: "Precio", field: "Precio" },
    { name: "Cantidad", field: "Cantidad" }
    ]

    const tableData = React.useMemo(() => {
        let computedTable = productos;
        if (search) {
            computedTable = computedTable.filter(
                table => table.Nombre_Producto.toLowerCase().includes(search.toLowerCase()) ||
                    table.Codigo_SKU.toLowerCase().includes(search.toLowerCase())
            )
        }
        setTotalItems(computedTable.length)

        //Current page slice
        return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
    }, [productos, currentPage, search])


    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {
                const db = firebase.firestore()
                const data = await db.collection('registros').get()
                const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
                setProductos(arrayData)
                setLoading(false)

            } catch (error) {
                console.log(error)
            }
        }

        obtenerDatos()
        // eslint-disable-next-line
    }, [])


    function ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return (false)
    }

    const añadirFactura = async () => {
        if (!cedula || cedula.length !== 11) {
            toast.warning('Ingrese una cédula valida', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!userName) {
            toast.warning('Ingrese un nombre', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!phone || phone.length !== 9) {
            toast.warning('Ingrese una télefono valido', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!email) {
            toast.warning('Ingrese un correo', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!ValidateEmail(email)) {
            toast.warning('Formato de correo incorrecto', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (productosFactura.length === 0) {
            toast.warning('Se deben ingresar productos', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        let numeroFactura = shortid.generate()

        const factura = {
            idFactura: numeroFactura,
            cedula: cedula,
            nombre: userName,
            telefono: phone,
            correo: email,
            envio: envio,
            direccionEnvio: direccionEnvio,
            informacionAdicional: informacionAdicional,
            datosFactura: productosFactura,
            precioTotal: parseInt(total),
            fechaCreacion: Date.now()
        }

        let db = firebase.firestore()
        try {


            productosFactura.map(async (item) => {
                const data = await db.collection('registros').doc(item.idProduct).get()
                if (item.value > data.data().Unidad_Disponibles) {
                    toast.warning('No hay inventario disponible de : ' + item.name, {
                        position: "top-center",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    return
                }
                await db.collection('registros').doc(item.idProduct).update({
                    Unidad_Disponibles: data.data().Unidad_Disponibles - item.value
                })
            }
            )

            if (envio === 'Si') {
                const nuevoEnvio = {
                    Confirmado: "Si",
                    Destino: direccionEnvio,
                    InformacionAdicional: informacionAdicional,
                    Estado: "En espera",
                    Recibe: userName,
                    Cedula: cedula,
                    Estados: [{
                        Estado: "En espera",
                        Fecha: Date.now()
                    }],
                    FechaCreacion: Date.now(),
                    NumeroFactura: numeroFactura
                }

                await db.collection('envios').add(nuevoEnvio)
            }


        } catch (error) {
            console.log(error)
        }

        try {
            await db.collection('facturas').add(factura)
            toast.info('Factura añadida correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } catch (error) {
            console.log(error)
        }



        setEmail('')
        setEnvio('No')
        setPhone('')
        setUserName('')
        setCedula('')
        setDireccionEnvio('')
        setInformacionAdicional('')
        setProductosFactura([])
        setTotal(0)


        props.history.push("/VerFacturas")


    }

    const addProduct = (value, newItem) => {
        let productFound = false

        productosFactura.map(item => (
            item.id === newItem.Codigo_SKU ? productFound = true : null
        ))

        if (productFound) {
            let arrayEditado = {}
            if (value === '' || value === '0' || value === 0) {
                arrayEditado = productosFactura.filter(item => item.id !== newItem.Codigo_SKU)
            } else {
                arrayEditado = productosFactura.map((item, index) => (
                    item.id === newItem.Codigo_SKU ? { id: newItem.Codigo_SKU,idProduct: newItem.id, value: value, name: newItem.Nombre_Producto, price: value * newItem.Precio } : item
                ))
            }
            if (value > newItem.Unidad_Disponibles) {
                toast.warning('Cantidad de productos disponibles: ' + newItem.Unidad_Disponibles, {
                  position: "top-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                return
              }
            setProductosFactura(arrayEditado)
        } else {
            if (value > newItem.Unidad_Disponibles) {
                toast.warning('Cantidad de productos disponibles: ' + newItem.Unidad_Disponibles, {
                  position: "top-center",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                return
              }
            setProductosFactura([...productosFactura,
            { id: newItem.Codigo_SKU, idProduct: newItem.id,value: value, name: newItem.Nombre_Producto, price: value * newItem.Precio }
            ])

        }
    }



    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col">
                </div>
                <div className="col-10">
                    <Modal show={show} onHide={handleClose} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Productos</Modal.Title>
                            <Button variant="secondary" onClick={handleClose}>
                                Cerrar
                            </Button>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="table-responsive">
                                <ToastContainer />
                                <div className="mb-3">
                                    <Search onSearch={(value) => {
                                        setSearch(value)
                                        setCurrentPage(1)
                                    }
                                    }
                                    />
                                </div>
                                <table className="table mt-4">
                                    <TableHeader headers={headers} />
                                    <tbody>
                                        {
                                            tableData.map((item, index) => (
                                                <tr key={index}>
                                                    <td>{item.Codigo_SKU}</td>
                                                    <td>{item.Nombre_Producto}</td>
                                                    <td>₡ {item.Precio}</td>
                                                    <td><input type="number" className="form-control" id="cantidadProducto" autocomplete="off" onChange={e => addProduct(e.target.value, item)} /></td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </Modal.Body>
                    </Modal>
                    <div className="card">
                        <h5 className="card-title mx-3 mt-3">Facturación</h5>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label for="exampleFormControlInput1">Cédula</label>
                                    <InputMask mask="9-9999-9999" maskChar="" type="text" class="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setCedula(e.target.value)} value={cedula} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleFormControlInput1">Nombre</label>
                                    <InputMask type="text" className="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setUserName(e.target.value)} value={userName} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleFormControlInput1">Télefono</label>
                                    <InputMask mask="9999-9999" maskChar="" type="text" className="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setPhone(e.target.value)} value={phone} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleFormControlInput1">Correo</label>
                                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="correo@ejemplo.com" autocomplete="off" onChange={e => setEmail(e.target.value)} value={email} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleFormControlSelect1">Envio</label>
                                    <select className="form-control" id="exampleFormControlSelect1" autocomplete="off" onChange={e => setEnvio(e.target.value)} value={envio}>
                                        <option>No</option>
                                        <option>Si</option>
                                    </select>
                                </div>

                                {envio === 'Si' ?
                                    <>
                                        <div className="form-group">
                                            <label for="exampleFormControlSelect1">Dirección envio</label>
                                            <textarea type="text" className="form-control" id="dirrecionEnvioInput" autocomplete="off" onChange={e => setDireccionEnvio(e.target.value)} value={direccionEnvio} />
                                        </div>
                                        <div className="form-group">
                                            <label for="exampleFormControlSelect1">Información adicional</label>
                                            <textarea type="text" className="form-control" id="informacionAdicionalEnvioInput" autocomplete="off" onChange={e => setInformacionAdicional(e.target.value)} value={informacionAdicional} />
                                        </div>
                                    </>
                                    : null}
                                <div className="form-group">
                                    <label htmlFor="fromValue">Productos</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary" onClick={() => handleShow('From')} type="button" id="button-addon1"><i class="fas fa-search-plus"></i></button>
                                        </div>
                                        <input type="text" autoComplete="off" className="form-control" id="fromValue" readOnly />
                                    </div>
                                </div>
                                <table className="table mt-4">
                                    <TableHeader headers={headers} />
                                    <tbody>
                                        {
                                            productosFactura.map((item, index) => (
                                                <tr key={index}>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.value}</td>
                                                    <td>₡ {item.price}</td>
                                                </tr>
                                            ))
                                        }
                                        <td>Total: ₡ {total}</td>
                                    </tbody>
                                </table>
                                <button type="button" className="btn btn-primary mt-2" onClick={() => añadirFactura()}>Guardar</button>
                                <Link type="button" to="/VerFacturas" className="btn btn-secondary mt-2 ml-2">Cancelar</Link>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col">
                </div>
            </div>
        </div>
    )
}

export default withRouter(Create)