import React from 'react'
import { firebase } from '../../firebase'
import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'
import { Button, Modal, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { margin } from '@mui/system';
import moment from 'moment'
import 'moment/locale/es' // Pasar a español

// Table search and pagination
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'
import Pagination from '../Datatable/Pagination'

//Shortid
import shortid from 'shortid';

const Facturarventas = (props) => {
    const dispatch = useDispatch()

    const usuario = useSelector(store => store.usuario.user)

    const [ventas, setVentas] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    //Editar

    const [codigo, setCodigo] = React.useState('');
    const [nombre, setNombre] = React.useState('');
    const [precio, setPrecio] = React.useState('');
    const [unidades, setUnidades] = React.useState('');
    const [id, setId] = React.useState();

    //Tabla
    const [mostrarProductos, setMostrarProductos] = React.useState(false);
    const [datosFactura, setDatosFactura] = React.useState([]);
    const mostrarMenos = () => setMostrarProductos(false)

    // Table search and pagination
    const ITEMS_PER_PAGE = 5;
    const [totalItems, setTotalItems] = React.useState(0);
    const [currentPage, setCurrentPage] = React.useState();
    const [search, setSearch] = React.useState('')

    // Table search and pagination
    const tableData = React.useMemo(() => {

        let computedTable = ventas;
        if (search) {
            computedTable = computedTable.filter(
                table => table.cedula.toLowerCase().includes(search.toLowerCase()) || table.nombre.toLowerCase().includes(search.toLowerCase())
            )
        }
        setTotalItems(computedTable.length)

        // Current page slice
        return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
    }, [ventas, currentPage, search])

    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {
                const db = firebase.firestore()
                const data = await db.collection('ventas').where("Facturacion", "==", "Pendiente").get()
                console.log(data.docs)
                const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
                console.log(arrayData)
                setVentas(arrayData)
                setLoading(false)
                setCurrentPage(1)

            } catch (error) {
                console.log(error)
            }
        }

        obtenerDatos()
        // eslint-disable-next-line
    }, [])

    const eliminar = async (id) => {
        try {
            const db = firebase.firestore();
            await db.collection('ventas').doc(id).delete();

            const arrayFiltrado = ventas.filter(item => item.id !== id);

            setVentas(arrayFiltrado);

            toast.success('La venta se ha eleiminado correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });


        } catch (error) {
            console.log(error);
        }
    };

    const modoEdicion = (item) => {
        setCodigo(item.Codigo)
        setNombre(item.Nombre)
        setPrecio(item.Precio)
        setUnidades(item.Unidades)
        setId(item.id);
        handleShow()
    };

    const editar = async () => {

        if (!nombre || !precio || !unidades || !codigo) {
            toast.warning('Ingrese todos los datos', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
        try {

            const db = firebase.firestore()
            await db.collection('ventas').doc(id).update({
                Codigo: codigo,
                Nombre: nombre,
                Precio: precio,
                Unidades: unidades,

            });
            const arrayEditado = ventas.map(item => (
                item.id === id ? { id: item.id, Codigo: codigo, Nombre: nombre, Precio: precio, Unidades: unidades } : item
            ));

            setVentas(arrayEditado)
            setId('')
            handleClose()
            props.history.push('/Ventas')

        } catch (error) {
            console.log(error)
        }

    }

    const mostrar = async (item) => {
        setDatosFactura(item)
        setMostrarProductos(true)
    }

    const facturar = async (item) => {
        let idFactura = shortid.generate()
        const factura = {
            idFactura: idFactura,
            cedula: item.CedulaCliente,
            nombre: item.NombreCliente,
            telefono: item.NumeroCliente,
            correo: item.CorreoCliente,
            envio: item.EnvioCliente,
            direccionEnvio: item.DireccionEnvioCliente,
            informacionAdicional: item.InformacionAdicionalCliente,
            datosFactura: item.Productos,
            precioTotal: parseInt(item.Total),
            fechaCreacion: Date.now()
        }

        try {

            const db = firebase.firestore()

            await db.collection('ventas').doc(item.id).update({
                Vendedor: item.Vendedor,
                CedulaCliente: item.CedulaCliente,
                NombreCliente: item.NombreCliente,
                CorreoCliente: item.CorreoCliente,
                NumeroCliente: item.NumeroCliente,
                EnvioCliente: item.EnvioCliente,
                DireccionEnvioCliente: item.DireccionEnvioCliente,
                InformacionAdicionalCliente: item.InformacionAdicionalCliente,
                Productos: item.Productos,
                Facturacion: "Facturado",
                Total: parseInt(item.Total),
                Fecha: item.Fecha
            })


            await db.collection('facturas').add(factura)
            toast.info('Facturada correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            if (item.EnvioCliente === 'Si') {
                const nuevoEnvio = {
                    Confirmado: "Si",
                    Destino: item.DireccionEnvioCliente,
                    InformacionAdicional: item.InformacionAdicionalCliente,
                    Estado: "En espera",
                    Recibe: item.NombreCliente,
                    Cedula: item.CedulaCliente,
                    Estados: [{
                        Estado: "En espera",
                        Fecha: Date.now()
                    }],
                    FechaCreacion: Date.now(),
                    NumeroFactura: idFactura
                }

                await db.collection('envios').add(nuevoEnvio)
            }

        } catch (error) {
            console.log(error)
        }



    }

    return (

        <div className="container mt-5">
            <div className="row">
                <div className="col">
                </div>
                <div className="col-10">
                    <Modal show={show} onHide={handleClose} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Editar Venta</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="container">
                                <div className="row">
                                    <div class="col">
                                        <Form>

                                            <Form.Group className="mb-3" controlId="formGridCodigo">
                                                <Form.Label>Código SKU</Form.Label>
                                                <Form.Control onChange={e => setCodigo(e.target.value)} value={codigo} />
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="formGridNombre">
                                                <Form.Label>Nombre del Producto</Form.Label>
                                                <Form.Control onChange={e => setNombre(e.target.value)} value={nombre} />
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="formGridPrecio">
                                                <Form.Label>Precio</Form.Label>
                                                <Form.Control type='number' onChange={e => setPrecio(e.target.value)} value={precio} />
                                            </Form.Group>

                                            <Form.Group className="mb-3" controlId="formGridUnidades">
                                                <Form.Label>Unidades Vendidas</Form.Label>
                                                <Form.Control type='number' onChange={e => setUnidades(e.target.value)} value={unidades} />
                                            </Form.Group>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button to="Dashboard" type="button" onClick={() => editar()} className="btn btn-primary ">Guardar</button>

                            <Button variant="secondary" onClick={handleClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <Modal show={mostrarProductos} onHide={mostrarMenos} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Productos</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="table-responsive">
                                <table className="table mt-4">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Precio</th>
                                            <th scope="col">Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            datosFactura.map((item, index) => (
                                                <tr key={index}>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.price}</td>
                                                    <td>{item.value}</td>
                                                </tr>
                                            ))
                                        }


                                    </tbody>
                                </table>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={mostrarMenos}>
                                Cerrar
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    {/* FIN MODAL */}

                    <div class="clearfix">
                        <div class="pull-left">
                            <div class="c-content-title-1">
                                <h3 class="font-weight-bold">Lista de Ventas</h3>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <Link type="button" to="/VerFacturas" class="btn btn-info mb-3"> Volver</Link>
                        </div>
                    </div>

                    <table class="table table-hover table-striped tablas " id="tablas">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Facturado</th>
                                <th scope="col">Fecha creación</th>
                                <th scope="col">Productos</th>
                                <th scope="col">Total vendido</th>
                                <th class="text-center" scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableData.map((item, index) => (
                                <tr key={index}>
                                    <th scope="row">{index}</th>
                                    <td>{item.NombreCliente}</td>
                                    <td>{item.CorreoCliente}</td>
                                    <td>{item.NumeroCliente}</td>
                                    <td>{item.Facturacion}</td>
                                    <td>{moment(item.Fecha).format('MMMM DD YYYY, h:mm:ss a')}</td>
                                    <td><button type="button" class="btn btn-info btn-sm" onClick={() => mostrar(item.Productos)}>Mostrar</button></td>
                                    <td>₡ {item.Total}</td>
                                    <td class="btn-group d-flex justify-content-center ">
                                        <button type="button" class="btn btn-info btn-sm" onClick={() => facturar(item)}>Facturar</button>
                                        <button type="button" class="btn btn-danger btn-sm" onClick={() => eliminar(item.id)}>Eliminar</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <Pagination
                        total={totalItems}
                        itemsPerPage={ITEMS_PER_PAGE}
                        currentPage={currentPage}
                        onPageChange={page => setCurrentPage(page)}
                    />
                    {loading && <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>}
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    )
}

export default withRouter(Facturarventas)