import React from 'react'
import { firebase } from '../../firebase'
import { useDispatch, useSelector } from 'react-redux'
import Chart from 'react-apexcharts'
import moment from 'moment'
import 'moment/locale/es' // Pasar a español


const Graficosvendedor = () => {

    const [loading, setLoading] = React.useState(true)
    const [totalVentas, setTotalVentas] = React.useState(0)
    const [totalVentasFacturadas, setTotalVentasFacturadas] = React.useState(0)
    const [gananciaFacturaMes, setGananciaFacturaMes] = React.useState([]);
    const [meses, setMeses] = React.useState([]);
    const [onlyProductos, setOnlyProductos] = React.useState([])
    const [onlyQtyProductos, setQtyOnlyProductos] = React.useState([])
    const usuario = useSelector(store => store.usuario.user)

    const [envios, setEnvios] = React.useState([])
    const [ventas, setVentas] = React.useState([])
    const [ingresos, setIngresos] = React.useState([])

    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {
                let getData = []
                let ventas = []
                let arrayData = []
                setLoading(true)
                const db = firebase.firestore();
                getData = await db.collection('ventas').where("Vendedor", "==", usuario.email).get();
                setTotalVentas(getData.docs.length)
                arrayData = getData.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                let filtered = arrayData.filter(a => a.Facturacion == "Facturado");
                setTotalVentasFacturadas(filtered.length)

                ventas = arrayData


                getData = await db.collection('envios').get();
                setEnvios(getData.docs.length)

                getData = await db.collection('ventas').get();
                setVentas(getData.docs.length)

                let totalIngresos = 0;
                ventas.map(async (item, index) => {
                    totalIngresos = totalIngresos + item.Total
                })
                setIngresos(totalIngresos)

                let meses = []
                let totalPorMes = []
                let total = 0
                meses.push("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
                meses.map(async (mes, mesIndex) => {
                    total = 0
                    ventas.map(async (item, index) => {
                        if (mes === moment(item.Fecha).format('MM')) {
                            total = total + item.Total
                        }
                    })
                    totalPorMes.push(total)
                })
                setMeses(meses)
                setGananciaFacturaMes(totalPorMes)


                let arrayOnlyProductos = []
                let arrayQtyProductos = []
                ventas.map(async (item, index) => {
                    item.Productos.map(async (productos, indexProductos) => {
                        let producExists = false
                        arrayOnlyProductos.map(async (producto, indexProducto) => {
                            if (productos.name === producto) {
                                arrayOnlyProductos[indexProducto] = productos.name
                                arrayQtyProductos[indexProducto] = arrayQtyProductos[indexProducto] + parseInt(productos.value)
                                producExists = true
                            }
                        })
                        if (!producExists) {
                            arrayOnlyProductos.push(productos.name)
                            arrayQtyProductos.push(parseInt(productos.value))
                        }

                    })
                })


                setOnlyProductos(arrayOnlyProductos)
                setQtyOnlyProductos(arrayQtyProductos)


                setLoading(false);



            } catch (error) {
                console.log(error);
            }
        };

        obtenerDatos();

    }, []);


    const graficoBarrasGanancias = {
        options: {
            chart: {
                id: 'apexchart-example',
                height: 350
            },
            xaxis: {
                categories: meses
            }
        },
        series: [{
            name: 'series-1',
            data: gananciaFacturaMes
        }]
    }

    const productosPie = {

        series: onlyQtyProductos,
        options: {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: onlyProductos,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        },


    };


    return (


        <div className="container mt-5">

            <div class="clearfix ">
                <div class="pull-left">
                    <div class="c-content-title-1">
                        <h3 class="font-weight-bold">Panel de control</h3>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col">
                </div>
                <div className="col-12">
                    {!loading ?
                        <>
                            <div class="row mt-4">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="small-box bg-info">
                                        <div class="inner">
                                            <h3>  {!loading && ventas}</h3>
                                            <p>Ventas</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3>{!loading && totalVentasFacturadas}</h3>
                                            <p>Ventas Facturadas</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-stats-bars"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-3 col-">
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            <h3>
                                            ₡{!loading && ingresos}
                                            </h3>
                                            <p>Ingresos</p>
                                        </div>
                                        <div class="icon">
                                            <i class=" ion ion-stats-bars"> </i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Ganancias facturadas por mes</h5>
                                            {!loading && <Chart options={graficoBarrasGanancias.options} series={graficoBarrasGanancias.series} type="bar" width={1000} height={320} />}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Productos</h5>
                                            {!loading && <Chart options={productosPie.options} series={productosPie.series} type="pie" width={600} />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                        :
                        <div class="d-flex justify-content-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    }
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    )
}


export default Graficosvendedor