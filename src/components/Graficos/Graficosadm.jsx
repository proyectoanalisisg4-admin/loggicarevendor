import React from 'react'
import { firebase } from '../../firebase'
import Chart from 'react-apexcharts'
import moment from 'moment'
import 'moment/locale/es' // Pasar a español

const Graficosadm = () => {

    const [onlyProductos, setOnlyProductos] = React.useState([])
    const [onlyQtyProductos, setQtyOnlyProductos] = React.useState([])
    const [totalUsuarios, setTotalUsuarios] = React.useState();
    const [totalProductos, setTotalProductos] = React.useState();
    const [totalProductosInventario, setTotalProductosInventario] = React.useState();
    const [totalFacturas, setTotalFacturas] = React.useState();
    const [gananciaFacturaMes, setGananciaFacturaMes] = React.useState([]);
    const [meses, setMeses] = React.useState([]);
    const [enviosEspera, setEnviosEspera] = React.useState();
    const [enviosEmpacado, setEnviosEmpacado] = React.useState();
    const [enviosEnRuta, setEnviosEnRuta] = React.useState();
    const [enviosEntregado, setEnviosEntregado] = React.useState(0);
    const [loading, setLoading] = React.useState(true);


    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {

                setLoading(true)
                let getData = ''
                let arrayData = []
                let facturas = []
                let productos = []
                const db = firebase.firestore();
                getData = await db.collection('usuarios').get();
                setTotalUsuarios(getData.docs.length)
                getData = await db.collection('registros').get();
                arrayData = getData.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                productos = arrayData
                setTotalProductos(getData.docs.length)
                getData = await db.collection('facturas').get();
                arrayData = getData.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                facturas = arrayData
                setTotalFacturas(getData.docs.length)
                getData = await db.collection('envios').where("Estado", "==", "En espera").get();
                setEnviosEspera(getData.docs.length)
                getData = await db.collection('envios').where("Estado", "==", "Empacando").get();
                setEnviosEmpacado(getData.docs.length)
                getData = await db.collection('envios').where("Estado", "==", "En ruta").get();
                setEnviosEnRuta(getData.docs.length)
                getData = await db.collection('envios').where("Estado", "==", "Entregado").get();
                setEnviosEntregado(getData.docs.length)

                let arrayOnlyProductos = []
                let arrayQtyProductos = []
                let totalProductosInventario = 0
                productos.map(async (item, index) => {
                    arrayOnlyProductos.push(item.Nombre_Producto)
                    arrayQtyProductos.push(item.Unidad_Disponibles)
                    totalProductosInventario = totalProductosInventario + item.Unidad_Disponibles
                })
                setOnlyProductos(arrayOnlyProductos)
                setQtyOnlyProductos(arrayQtyProductos)
                setTotalProductosInventario(totalProductosInventario)

                let meses = []
                let totalPorMes = []
                let total = 0
                meses.push("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
                meses.map(async (mes, mesIndex) => {
                    total = 0
                    facturas.map(async (item, index) => {
                        if (mes === moment(item.fechaCreacion).format('MM')) {
                            total = total + item.precioTotal
                        }
                    })
                    totalPorMes.push(total)
                })
                setMeses(meses)
                setGananciaFacturaMes(totalPorMes)

                setLoading(false);



            } catch (error) {
                console.log(error);
            }
        };

        obtenerDatos();
        // eslint-disable-next-line
    }, []);

    const dashboardTest = {
        options: {
            chart: {
                id: 'apexchart-example',
                height: 350
            },
            xaxis: {
                categories: meses
            }
        },
        series: [{
            name: 'series-1',
            data: gananciaFacturaMes
        }]
    }

    const pieGra = {

        series: [enviosEspera, enviosEmpacado, enviosEnRuta, enviosEntregado],
        options: {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['En espera', 'Empacado', 'En ruta', 'Entregado'],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        },


    };

    const productosPie = {

        series: onlyQtyProductos,
        options: {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: onlyProductos,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        },


    };

    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col">
                </div>
                <div className="col-12">
                    <h3 class="font-weight-bold">Gráficos</h3>
                    {!loading ?
                        <>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card" Style="height: 8rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Usuarios registrados</h5>
                                            <h1>{!loading && totalUsuarios}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card" Style="height: 8rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Diferentes productos en inventario</h5>
                                            <h1>{!loading && totalProductos}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <div class="card" Style="height: 8rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Facturas creadas</h5>
                                            <h1>{!loading && totalFacturas}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card" Style="height: 8rem;">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Total productos en inventario</h5>
                                            <h1>{!loading && totalProductosInventario}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Ganancias facturadas por mes</h5>
                                            {!loading && <Chart options={dashboardTest.options} series={dashboardTest.series} type="bar" width={1000} height={320} />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Productos</h5>
                                            {!loading && <Chart options={productosPie.options} series={productosPie.series} type="pie" width={500} />}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <h5 class="card-title">Envíos</h5>
                                            {!loading && <Chart options={pieGra.options} series={pieGra.series} type="pie" width={380} />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                        :
                        <div class="d-flex justify-content-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    }
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    )
}

export default Graficosadm