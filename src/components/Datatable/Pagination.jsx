import React from 'react'
import { useMemo, useEffect } from 'react'
import Pagination from 'react-bootstrap/Pagination'

const PaginationComponent = ({ total = 0, itemsPerPage = 10, currentPage = 1, onPageChange }) => {

    const [totalPages, setTotalPages] = React.useState(0)


    useEffect(() => {
        if (total > 0 && itemsPerPage > 0) {
            setTotalPages(Math.ceil(total / itemsPerPage));
        }

    }, [total, itemsPerPage])

    const paginationItems = useMemo(() => {
        const pages = [];
        for (let i = 1; i <= totalPages; i++) {
            pages.push(
                <li class={i === currentPage ? 'page-item active' : 'page-item'} aria-current="page">
                    <a class="page-link" onClick={() => onPageChange(i)}>{i}</a>
                </li>
            )
        }

        return pages;
    }, [totalPages, currentPage, onPageChange])


    if (totalPages === 0) return null;

    return (
        <Pagination>
            <li class={currentPage === 1 ? 'page-item disabled' : 'page-item'}>
                <a class="page-link" onClick={() => onPageChange(currentPage - 1)}>Anterior</a>
            </li>
            {paginationItems}
            <li class={currentPage === totalPages ? 'page-item disabled' : 'page-item'}>
                <a class="page-link" onClick={() => onPageChange(currentPage + 1)}>Siguiente</a>
            </li>
        </Pagination>
    )
}

export default PaginationComponent
