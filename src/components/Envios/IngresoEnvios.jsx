import React from 'react'

//Redux
import { useDispatch, useSelector } from 'react-redux'
import { agregarEnvioAccion, authWithUserAndPassword, editarFotoAccion } from '../../redux/usuarioDucks'
import { Link } from 'react-router-dom'



import { withRouter } from 'react-router-dom'

//Logica BD importar Form
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { auth, firebase, db, storage } from '../../firebase'


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';




const IngresoEnvios = (props) => {
  const dispatch = useDispatch()

  //Logica para agregar a base datos de la info del formulario
  const [confirmado, setConfirmado] = React.useState('')
  const [destino, setDestino] = React.useState('')
  const [estado, setEstado] = React.useState('')
  const [recibe, setRecibe] = React.useState('')
  const [numeroFactura, setNumeroFactura] = React.useState('')
  //Fin BD


  //Logica para agregar a base datos de la info del formulario
  const agregar = async (e) => {
    e.preventDefault()

    if (!destino.trim() || !recibe.trim() || !numeroFactura.trim()) {
      toast.warning('Ingrese todos los datos', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return
    }

    try {

      const db = firebase.firestore()
      const nuevoEnvio = {
        Confirmado: confirmado,
        Destino: destino,
        Estado: estado,
        Recibe: recibe,
        NumeroFactura: numeroFactura
      }

      const data = await db.collection('envios').add(nuevoEnvio)
      dispatch(agregarEnvioAccion(data.id))

    } catch (error) {
      console.log(error)
    }
    toast.success('Envío creado', {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    e.target.reset();
    return
  }
  // Fin de logica base de datos

  return (
    <>

      <div className="container mt-5">
        <div className="row">
          <div className="col">
          </div>
          <div className="col-10">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"> Registrar Envíos</h5>
                <br />
                <Form onSubmit={agregar}>
                  <Form.Group className="mb-3" controlId="formGridNumeroFactura"
                    onChange={e => setNumeroFactura(e.target.value)} value={numeroFactura}>
                    <Form.Label>Número de factura</Form.Label>
                    <Form.Control placeholder="FC00000000000" autocomplete="off" />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGridDestino"
                    onChange={e => setDestino(e.target.value)} value={destino}>
                    <Form.Label>Destino</Form.Label>
                    <Form.Control placeholder="San José, San Pedro..." autocomplete="off" />
                  </Form.Group>
                  <Form.Group class="form-group">
                    <label for="formGridConfirmado">Confirmado</label>
                    <select class="form-control" id="formGridConfirmado" autocomplete="off" onChange={e => setConfirmado(e.target.value)} value={confirmado}>
                    <option value="" disabled selected hidden>Selecciona una opción...</option>
                      <option >Sí</option>
                      <option>No</option>
                    </select>
                  </Form.Group>
                  <Form.Group class="form-group">
                    <label for="formGridEstado">Estado de la orden</label>
                    <select class="form-control" id="formGridEstado"  onChange={e => setEstado(e.target.value)} value={estado}>
                    <option value="" disabled selected hidden>Selecciona un estado...</option>
                      <option >En espera</option>
                      <option>Empacando</option>
                      <option>Despachado</option>
                      <option>En ruta</option>
                      <option>Entregado</option>
                      <option>Orden cancelada</option>
                    </select>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGridRecibe"
                    onChange={e => setRecibe(e.target.value)} value={recibe}>
                    <Form.Label>Recibe</Form.Label>
                    <Form.Control placeholder="Agustin Santamaría..." autocomplete="off" />
                  </Form.Group>
                  <br />
                  <button to="Dashboard" type="submit" className="btn btn-primary mr-3">Guardar</button>
                  <Link to="/Envios" className="btn btn-secondary">Cancelar</Link>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

export default withRouter(IngresoEnvios)

