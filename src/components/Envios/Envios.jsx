import React from 'react'
import { firebase } from '../../firebase'
import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'
import { Button, Modal, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import 'moment/locale/es' // Pasar a español

// Table search and pagination
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'
import Pagination from '../Datatable/Pagination'

const Envios = (props) => {
  const dispatch = useDispatch()

  const [envios, setEnvios] = React.useState([])
  const [loading, setLoading] = React.useState(true)

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const [showEstados, setShowEstados] = React.useState(false);

  const handleCloseEstados = () => setShowEstados(false)


  //Editar
  const [confirmado, setConfirmado] = React.useState('')
  const [destino, setDestino] = React.useState('')
  const [estado, setEstado] = React.useState('')
  const [recibe, setRecibe] = React.useState('')
  const [numeroFactura, setNumeroFactura] = React.useState('')
  const [id, setId] = React.useState()
  const [numFactura, setNumFactura] = React.useState()
  const [estados, setEstados] = React.useState([])
  const [estadoAnterior, setEstadoAnterior] = React.useState([])


  // Table search and pagination
  const ITEMS_PER_PAGE = 5;
  const [totalItems, setTotalItems] = React.useState(0);
  const [currentPage, setCurrentPage] = React.useState();
  const [search, setSearch] = React.useState('')

  // Table search and pagination
  const tableData = React.useMemo(() => {

    let computedTable = envios;
    if (search) {
      computedTable = computedTable.filter(
        table => table.Estado.toLowerCase().includes(search.toLowerCase()) || table.NumeroFactura.toString().includes(search.toString())
      )
    }
    setTotalItems(computedTable.length)

    // Current page slice
    return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
  }, [envios, currentPage, search])

  React.useEffect(() => {

    const obtenerDatos = async () => {
      try {
        const db = firebase.firestore()
        const data = await db.collection('envios').get()
        const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
        console.log(arrayData)
        setEnvios(arrayData)
        setLoading(false)
        setCurrentPage(1)

      } catch (error) {
        console.log(error)
      }
    }

    obtenerDatos()
    // eslint-disable-next-line
  }, [])

  const modoEdicion = (item) => {
    setConfirmado(item.Confirmado)
    setDestino(item.Destino)
    setEstado(item.Estado)
    setNumeroFactura(item.NumeroFactura)
    setRecibe(item.Recibe)
    setEstadoAnterior(item.Estados)
    setEstado(item.Estado)
    setId(item.id)
    handleShow()
  }
  const editar = async () => {

    if (!destino || !recibe || !numeroFactura) {
      toast.warning('Ingrese todos los datos', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return
    }
    try {

      estadoAnterior.push({ Estado: estado, Fecha: Date.now() })

      console.log(estado)
      const db = firebase.firestore()
      await db.collection('envios').doc(id).update({
        Confirmado: confirmado,
        Destino: destino,
        Estado: estado,
        Estados: estadoAnterior,
        Recibe: recibe,
        NumeroFactura: numeroFactura
      })
      const arrayEditado = envios.map(item => (
        item.id === id ? { id: item.id, Confirmado: confirmado, Destino: destino, Estado: estado, Recibe: recibe, NumeroFactura: numeroFactura, Estados: estadoAnterior } : item
      ))

      setEnvios(arrayEditado)
      setId('')
      handleClose()
      props.history.push('/Envios')

    } catch (error) {
      console.log(error)
    }

  }

  const handleShowEstados = async (item) => {
    setShowEstados(true)
    setEstados(item)
  }




  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col">
        </div>
        <div className="col-10">
          <Modal show={show} onHide={handleClose} className="mt-5">
            <Modal.Header>
              <Modal.Title>Editar envío</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="container">
                <div className="row">
                  <div class="col">
                    <Form>
                      <Form.Group className="mb-3" controlId="formGridNumeroFactura">
                        <Form.Label>Número de Factura</Form.Label>
                        <Form.Control onChange={e => setNumeroFactura(e.target.value)} value={numeroFactura} type='text' min='1' />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGridDestino">
                        <Form.Label>Destino</Form.Label>
                        <Form.Control onChange={e => setDestino(e.target.value)} value={destino} />
                      </Form.Group>
                      <Form.Group class="form-group">
                        <label for="formGridEstado">Estado de la orden</label>
                        <select class="form-control" id="formGridEstado" autocomplete="off" onChange={e => setEstado(e.target.value)} value={estado}>
                          <option> </option>
                          <option>En espera</option>
                          <option>Empacando</option>
                          <option>Despachado</option>
                          <option>En ruta</option>
                          <option>Entregado</option>
                          <option>Orden cancelada</option>
                        </select>
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGridRecibe">
                        <Form.Label>Recibe</Form.Label>
                        <Form.Control onChange={e => setRecibe(e.target.value)} value={recibe} />
                      </Form.Group>
                      <Form.Group class="form-group">
                        <label for="formGridConfirmado">Confirmado</label>
                        <select class="form-control" id="formGridConfirmado" autocomplete="off" onChange={e => setConfirmado(e.target.value)} value={confirmado}>
                          <option> </option>
                          <option>Si</option>
                          <option>No</option>
                        </select>
                      </Form.Group>

                    </Form>
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button to="Dashboard" type="button" onClick={() => editar()} className="btn btn-primary ">Guardar</button>
              <Button variant="secondary" onClick={handleClose}>
                Cancelar
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={showEstados} onHide={handleCloseEstados} className="mt-5">
            <Modal.Header>
              <Modal.Title>Editar envío</Modal.Title>
            </Modal.Header>
            <Modal.Body>

              <table class="table table table-hover table-striped tablas">
                <thead>
                  <tr>
                    <th scope="col">Estado</th>
                    <th scope="col">Fecha</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    estados.map((item, index) => (
                      <tr key={index}>
                        <td>{item.Estado}</td>
                        <td>{moment(item.Fecha).format('MMMM DD YYYY, h:mm:ss a')}</td>
                      </tr>
                    ))
                  }
                </tbody>
              </table>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleCloseEstados}>
                Cancelar
              </Button>
            </Modal.Footer>
          </Modal>

          <div class="clearfix">
            <div class="pull-left">
              <div class="c-content-title-1">
                <h3 class="font-weight-bold">Lista de envíos</h3>
              </div>
            </div>
          </div>
          <div class="pull-right">
            <div class="btn-group">
            </div>
          </div>

          <Search onSearch={(value) => {
            setSearch(value)
            setCurrentPage(1)
          }
          }
          />

          <table class="table table table-hover table-striped tablas mt-4">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Destino</th>
                <th scope="col">Estado de la orden</th>
                <th scope="col">Estados</th>
                <th scope="col">Orden Confirmada</th>
                <th scope="col">Recibe</th>
                <th scope="col">Número de factura</th>
                <th scope="col">Acción</th>
              </tr>
            </thead>
            <tbody>
              {
                tableData.map((item, index) => (
                  <tr key={index}>
                    <th scope="row">{index}</th>
                    <td>{item.Destino}</td>
                    <td>{item.Estado}</td>
                    <td><button className='btn btn-primary' onClick={() => handleShowEstados(item.Estados)}>Ver</button></td>
                    <td>{item.Confirmado}</td>
                    <td>{item.Recibe}</td>
                    <td>{item.NumeroFactura}</td>
                    <td>
                      <Button variant="warning" className="mr-2" onClick={() => modoEdicion(item)}>
                        Editar
                      </Button>


                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          <Pagination
            total={totalItems}
            itemsPerPage={ITEMS_PER_PAGE}
            currentPage={currentPage}
            onPageChange={page => setCurrentPage(page)}
          />
          {loading && <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>}
        </div>
        <div class="col">
        </div>
      </div>
    </div>
  )
}

export default withRouter(Envios)