import React from 'react'

//Redux
import { useDispatch, useSelector } from 'react-redux'
import { authWithUserAndPassword, editarFotoAccion } from '../../redux/usuarioDucks'
import { Link } from 'react-router-dom'



import { withRouter } from 'react-router-dom'

//Logica BD importar Form
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { auth, firebase, db, storage } from '../../firebase'


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';




const Formuario_ingreso = (props) => {
  const dispatch = useDispatch()

  const loading = useSelector(store => store.usuario.loading) //Get loading from usuarioDucks
  const activo = useSelector(store => store.usuario.activo) //Get loading from usuarioDucks


  //Logica para agregar a base datos de la info del formulario
  const [resgistro, setRegistro] = React.useState('')
  const [codigoSKU, setCodigo] = React.useState('')
  const [precio, setPrecio] = React.useState('')
  const [unidades, setUnidades] = React.useState('')
  const [visible, setVisible] = React.useState('')
  const [image, setImage] = React.useState('')
  //Fin BD


  //Logica para agregar a base datos de la info del formulario
  const agregar = async (e) => {
    e.preventDefault()

    if (!resgistro.trim() || !codigoSKU.trim() || !precio.trim() || !unidades.trim()) {
      toast.warning('Ingrese todos los datos', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return
    }

    try {

      const db = firebase.firestore()

      if (image === undefined) {
        console.log("Incorrecto")
        return
      }

      if (image.type === "image/png" || image.type === "image/jpg" || image.type === "image/jpeg") {

        let imagenURL = ''
        try {
          const imagenRef = storage.ref().child(codigoSKU).child('portada')
          await imagenRef.put(image)
          imagenURL = await imagenRef.getDownloadURL()
        } catch (error) {
          console.log(error)
        }

        const nuevoRegistro = {
          Nombre_Producto: resgistro,
          Codigo_SKU: codigoSKU,
          Precio: parseInt(precio),
          Unidad_Disponibles: parseInt(unidades),
          Visible: visible,
          photoURL: imagenURL
        }


        await db.collection('registros').add(nuevoRegistro)
        toast.success('Imagen agregada', {
          position: "top-center",
          autoClose: 1500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.warning('Solo archivos .png o .jpg', {
          position: "top-center",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return
      }


      setRegistro('')


    } catch (error) {
      console.log(error)
    }
    toast.success('Producto agregado', {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    e.target.reset();
    props.history.push("/Inventario")
    return
  }
  // Fin de logica base de datos

  //Logica de imagen
  const seleccionarArchivo = (imagen) => {
    setImage(imagen.target.files[0])
    const imagenCliente = imagen.target.files[0]
    console.log(imagenCliente)

    if (imagenCliente === undefined) {
      console.log("Incorrecto")
      return
    }

    if (imagenCliente.type === "image/png" || imagenCliente.type === "image/jpg" || imagenCliente.type === "image/jpeg") {
      console.log("cambiar")
      dispatch(editarFotoAccion(imagenCliente))
      toast.success('Imagen agregada', {
        position: "top-center",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      toast.warning('Solo archivos .png o .jpg', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return
    }
  }


  return (
    <>

      <div className="container mt-5">
        <div className="row">
          <div className="col">
          </div>
          <div className="col-10">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Registrar Producto</h5>
                <br />
                <Form onSubmit={agregar}>
                  <Form.Group className="mb-3" controlId="formGridNombre"
                    onChange={e => setRegistro(e.target.value)} value={resgistro}>
                    <Form.Label>Nombre del Producto</Form.Label>
                    <Form.Control placeholder="Shampoo, Acondicionador, etc..." autocomplete="off" />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGridCodigo"
                    onChange={e => setCodigo(e.target.value)} value={codigoSKU}>
                    <Form.Label>Código SKU</Form.Label>
                    <Form.Control placeholder="0001, 0002, etc..." type='number' />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGridPrecio"
                    onChange={e => setPrecio(e.target.value)} value={precio}>
                    <Form.Label>Precio</Form.Label>
                    <Form.Control type='number' />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGridUnidades"
                    onChange={e => setUnidades(e.target.value)} value={unidades}>
                    <Form.Label>Unidades Disponibles</Form.Label>
                    <Form.Control type='number' />
                  </Form.Group>


                  <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>Subir imagen</Form.Label>
                    <div className="custom-file">
                      <input
                        type="file"
                        className="custom-file-input"
                        id="inputGroupFile01"
                        style={{ display: 'none' }}
                        onChange={e => seleccionarArchivo(e)}
                        disabled={loading}
                      />
                      <label className={loading ? 'btn btn-dark mt-2 disabled' : 'btn btn-dark mt-2'}
                        htmlFor="inputGroupFile01">Buscar imagen</label>
                    </div>

                  </Form.Group>


                  <Form.Group className="mb-3" id="formGridCheckbox"
                    onChange={e => setVisible(e.target.value)} value={visible}>
                    <Form.Check type="checkbox" label="Visible" />
                  </Form.Group>
                  <div class="pull-ri">
                    <button to="Dashboard" type="submit" className="btn btn-primary mr-3" >Guardar</button>
                    <Link to="/Inventario" className="btn btn-secondary ">Cancelar</Link>
                  </div>


                </Form>


              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

export default withRouter(Formuario_ingreso)

