import React from 'react'
import { firebase,storage,db } from '../../firebase'
import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'
import { Button, Modal, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { editarFotoAccion } from '../../redux/usuarioDucks'
import { withRouter } from 'react-router-dom'
import 'datatables.net';

// Table search and pagination
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'
import Pagination from '../Datatable/Pagination'

function Inventario(props) {
  const dispatch = useDispatch();

  const foto = useSelector(store => store.usuario.foto);

  const [productos, setProductos] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //Editar
  const [image, setImage] = React.useState('');
  const [newImage, setNewImage] = React.useState('');
  const [nombre, setNombre] = React.useState('');
  const [codigo, setCodigo] = React.useState('');
  const [precio, setPrecio] = React.useState('');
  const [unidades, setUnidades] = React.useState('');
  const [visible, setVisible] = React.useState('');
  const [id, setId] = React.useState();

  // Table search and pagination
  const ITEMS_PER_PAGE = 5;
  const [totalItems, setTotalItems] = React.useState(0);
  const [currentPage, setCurrentPage] = React.useState();
  const [search, setSearch] = React.useState('')

  // Table search and pagination
  const tableData = React.useMemo(() => {
    let computedTable = productos;
    if (search) {
      computedTable = computedTable.filter(
        table => table.Codigo_SKU.toLowerCase().includes(search.toLowerCase()) || table.Nombre_Producto.toLowerCase().includes(search.toLowerCase())
      )
    }
    setTotalItems(computedTable.length)

    // Current page slice
    return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
  }, [productos, currentPage, search])

  React.useEffect(() => {

    const obtenerDatos = async () => {
      try {
        const db = firebase.firestore();
        const data = await db.collection('registros').get();
        const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }));
        console.log(arrayData);
        setProductos(arrayData);
        setLoading(false);
        setCurrentPage(1);

      } catch (error) {
        console.log(error);
      }
    };

    obtenerDatos();
    // eslint-disable-next-line
  }, []);

  const eliminar = async (id) => {
    try {
      const db = firebase.firestore();
      await db.collection('registros').doc(id).delete();

      const arrayFiltrado = productos.filter(item => item.id !== id);

      setProductos(arrayFiltrado);

      toast.success('Producto elimado correctamente', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });


    } catch (error) {
      console.log(error);
    }
  };

  const modoEdicion = (item) => {
    setNombre(item.Nombre_Producto);
    setPrecio(item.Precio);
    setUnidades(item.Unidad_Disponibles);
    setCodigo(item.Codigo_SKU);
    setVisible(item.Visible);
    setId(item.id);
    setImage(item.photoURL);
    handleShow();
    console.log(foto);
  };

  const editar = async () => {
    let imagenURL = image

    if (!nombre || !codigo || !precio || !unidades) {
      toast.warning('Ingrese todos los datos', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return;
    }
    try {
      console.log(nombre);
      if (newImage === undefined) {
        console.log("Incorrecto");
        return;
      }
      if (image === null) {
        if (newImage.type === "image/png" || newImage.type === "image/jpg" || newImage.type === "image/jpeg") {
          console.log("cambiar");
          try {
            const imagenRef = storage.ref().child(codigo).child('portada')
            await imagenRef.put(newImage)
            imagenURL = await imagenRef.getDownloadURL()
          } catch (error) {
            console.log(error)
         }

          toast.success('Imagen agregada', {
            position: "top-center",
            autoClose: 1500,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.warning('Ingrese una imagen valida solo archivos .png o .jpg', {
            position: "top-center",
            autoClose: 3000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return;
        }
      }
      const db = firebase.firestore();
      await db.collection('registros').doc(id).update({
        Codigo_SKU: codigo,
        Nombre_Producto: nombre,
        Precio: parseInt(precio),
        Unidad_Disponibles: parseInt(unidades),
        Visible: visible,
        photoURL: imagenURL
      });

      const arrayEditado = productos.map(item => (
        item.id === id ? { id: item.id, Codigo_SKU: codigo, Nombre_Producto: nombre, Precio: precio, Unidad_Disponibles: unidades, Visible: visible, photoURL: imagenURL } : item
      ));


      setProductos(arrayEditado);
      setId('');
      handleClose();
      props.history.push('/Inventario');
      toast.success('Producto editado correctamente', {
        position: "top-center",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });


    } catch (error) {
      console.log(error);
    }

  };

  const seleccionarArchivo = (newImageP) => {
    setNewImage(newImageP.target.files[0]);
    setImage(null);
  };

  return (


    <div className="container mt-5">
      <div className="row">
        <div className="col">
        </div>
        <div className="col-10">
          <Modal show={show} onHide={handleClose} className="mt-5">
            <Modal.Header>
              <Modal.Title>Editar Producto</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="container">
                <div className="row">
                  <div class="col">
                    <Form>
                      <Form.Group className="mb-3" controlId="formGridNombre">
                        <Form.Label>Nombre del Producto</Form.Label>
                        <Form.Control onChange={e => setNombre(e.target.value)} value={nombre} />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGridCodigo">
                        <Form.Label>Código SKU</Form.Label>
                        <Form.Control onChange={e => setCodigo(e.target.value)} value={codigo} type='number' />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGridPrecio">
                        <Form.Label>Precio</Form.Label>
                        <Form.Control type='number' onChange={e => setPrecio(e.target.value)} value={precio} />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="formGridUnidades">
                        <Form.Label>Unidades Disponibles</Form.Label>
                        <Form.Control type='number' onChange={e => setUnidades(e.target.value)} value={unidades} />
                      </Form.Group>
                      <Form.Group controlId="formFile" className="mb-3">
                        <div className="custom-file">
                          <input
                            type="file"
                            className="custom-file-input"
                            id="inputGroupFile01"
                            style={{ display: 'none' }}
                            onChange={e => seleccionarArchivo(e)}
                            disabled={loading} />
                          <label className={loading ? 'btn btn-dark mt-2 disabled' : 'btn btn-dark mt-2'}
                            htmlFor="inputGroupFile01">Buscar imagen</label>
                        </div>

                      </Form.Group>


                      <Form.Group className="mb-3" id="formGridCheckbox"
                        onChange={e => setVisible(e.target.value)} value={visible}>
                        <Form.Check type="checkbox" label="Visible" />
                      </Form.Group>

                    </Form>

                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button to="Dashboard" type="button" onClick={() => editar()} className="btn btn-primary ">Guardar</button>

              <Button variant="secondary" onClick={handleClose}>
                Cancelar
              </Button>
            </Modal.Footer>
          </Modal>

          <div class="clearfix">
            <div class="pull-left">
              <div class="c-content-title-1">
                <h3 class="font-weight-bold">Lista de Productos</h3>
              </div>
            </div>
          </div>

          <div class="pull-right">
            <div class="btn-group">
              <Link type="button" to="/AñadirProducto" class="btn btn-success mt-3 mb-3"><span class="fa fa-plus"></span> Registrar Producto</Link>
            </div>
          </div>

          <Search onSearch={(value) => {
            setSearch(value)
            setCurrentPage(1)
          }
          }
          />
          <table class="table table-hover table-striped tablas " id="tablas">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Código</th>
                <th scope="col">Nombre</th>
                <th scope="col">Precio</th>
                <th scope="col">Unidades</th>
                <th scope="col">Imagen</th>
                <th class="text-center" scope="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
              {tableData.map((item, index) => (
                <tr key={index}>
                  <th scope="row">{index}</th>
                  <td>{item.Codigo_SKU}</td>
                  <td>{item.Nombre_Producto}</td>
                  <td>{item.Precio}</td>
                  <td>{item.Unidad_Disponibles}</td>
                  <td><img className="img-fluid mb-2" src={item.photoURL} alt="Foto" style={{ width: "50px", borderRadius: "50px" }} /></td>
                  <td class="btn-group d-flex justify-content-center ">
                    <Button variant="warning" className="" onClick={() => modoEdicion(item)}>
                      Editar
                    </Button>
                    <button type="button" class="btn btn-danger btn-sm" onClick={() => eliminar(item.id)}>Eliminar</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Pagination
            total={totalItems}
            itemsPerPage={ITEMS_PER_PAGE}
            currentPage={currentPage}
            onPageChange={page => setCurrentPage(page)}
          />
          {loading && <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>}
        </div>
        <div class="col">
        </div>
      </div>
    </div>
  );
}


export default withRouter(Inventario)


