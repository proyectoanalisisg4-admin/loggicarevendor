import React from 'react'

//Redux
import { useDispatch, useSelector } from 'react-redux'
import { authWithUserAndPassword } from '../redux/usuarioDucks'
import { Link } from 'react-router-dom'

import { withRouter } from 'react-router-dom'


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = (props) => {
    const dispatch = useDispatch()

    const loading = useSelector(store => store.usuario.loading) //Get loading from usuarioDucks
    const activo = useSelector(store => store.usuario.activo) //Get loading from usuarioDucks
    const usuario = useSelector(store => store.usuario.user)

    const [email, setEmail] = React.useState('')
    const [pass, setPass] = React.useState('')
    const [rememberPassword, setRememberPassword] = React.useState()
    const [error, setError] = React.useState(null)

    React.useEffect(() => {
        console.log(activo)
        if (activo) {
            if (usuario.role === "Vendedor") {
                props.history.push('/GraficosVendedor')
            } else {
                props.history.push('/Graficos')
            }

        }
        if (localStorage.getItem('rememberUser')) {
            setEmail(localStorage.getItem('rememberUser'))
            setRememberPassword(true)
        } else {
            localStorage.removeItem('rememberUser');
        }
    }, [props, activo])

    function ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return (false)
    }
    const procesarDatos = e => {
        if (!email.trim()) {
            toast.warning('Ingrese su correo', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
        if (!pass.trim()) {
            toast.warning('Ingrese su contraseña', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!ValidateEmail(email)) {
            toast.warning('Formato de correo incorrecto', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (rememberPassword) {
            localStorage.setItem('rememberUser', email)
        } else {
            localStorage.removeItem("rememberUser");
            setEmail('')
            setPass('')
            setRememberPassword(false)
        }
        dispatch(authWithUserAndPassword(email, pass))
        setError(null)
    }

    const rememberUser = e => {
        if (e.target.checked) {
            setRememberPassword(true)
        } else {
            setRememberPassword(false)
        }
    }

    return (
        <>

            <div className="container mt-5">
                <ToastContainer />
                <div className="row">
                    <div className="col">

                    </div>
                    <div className="col-8">
                        <div className="card">
                            <div className="card-header text-center">
                                <img src="/assets/images/logo.png" Style="width:300px" className="img-fluid" alt="Logo loggicare" />
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1">Correo electrónico</label>
                                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setEmail(e.target.value)} value={email} />
                                        <small id="emailHelp" className="form-text text-muted">No compartas esta información con nadie más</small>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1">Contraseña</label>
                                        <input type="password" className="form-control" id="exampleInputPassword1" onChange={e => setPass(e.target.value)} value={pass} />
                                    </div>
                                    <div className="form-group">
                                        <Link to="OlvideContraseña" className="card-link">¿Olvidaste tu contraseña?</Link>
                                    </div>

                                    <div className="form-group form-check">
                                        <input type="checkbox" className="form-check-input" id="exampleCheck1" onChange={e => rememberUser(e)} defaultChecked={rememberPassword} />
                                        <label className="form-check-label" htmlFor="exampleCheck1">Recuerdame</label>
                                    </div>

                                    {loading ?
                                        <button class="btn btn-primary" type="button" disabled>
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            <span class="sr-only">Cargando...</span>
                                        </button>
                                        : <button to="Dashboard" type="button" className="btn btn-primary" onClick={() => procesarDatos()}>Ingresar</button>}
                                    <hr></hr>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col">

                    </div>
                </div>
            </div>
        </>

    )
}

export default withRouter(Login)
