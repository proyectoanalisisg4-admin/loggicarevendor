import React from 'react'
import { firebase } from '../../firebase'
import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'
import { Button, Modal, Form } from 'react-bootstrap';
import moment from 'moment'
import 'moment/locale/es' // Pasar a español

// Table search and pagination
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'
import Pagination from '../Datatable/Pagination'


const VerUsuario = () => {

    const [usuarios, setUsuarios] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    const [show, setShow] = React.useState(false);

    const [id, setId] = React.useState()
    const [cedula, setCedula] = React.useState()
    const [nombre, setNombre] = React.useState()
    const [correo, setCorreo] = React.useState()
    const [telefono, setTelefono] = React.useState()
    const [rol, setRol] = React.useState()
    const [foto, setFoto] = React.useState()
    const [userId, setUserId] = React.useState()
    const [fechaDeCreación, setFechaDeCreación] = React.useState()

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // Table search and pagination
    const ITEMS_PER_PAGE = 5;
    const [totalItems, setTotalItems] = React.useState(0);
    const [currentPage, setCurrentPage] = React.useState();
    const [search, setSearch] = React.useState('')

    // Table search and pagination
    const tableData = React.useMemo(() => {
        let computedTable = usuarios;
        if (search) {
            computedTable = computedTable.filter(
                table => table.cedula.toLowerCase().includes(search.toLowerCase()) || table.userName.toLowerCase().includes(search.toLowerCase()) || table.role.toLowerCase().includes(search.toLowerCase())
            )
        }
        setTotalItems(computedTable.length)

        // Current page slice
        return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
    }, [usuarios, currentPage, search])

    React.useEffect(() => {

        const obtenerDatos = async () => {
            try {
                const db = firebase.firestore()
                const data = await db.collection('usuarios').orderBy('date', 'desc').get()
                const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
                console.log(arrayData)
                setUsuarios(arrayData)
                setLoading(false)
                setCurrentPage(1)

            } catch (error) {
                console.log(error)
            }
        }

        obtenerDatos()
        // eslint-disable-next-line
    }, [])

    const eliminar = async (id) => {
        try {
            const db = firebase.firestore()
            await db.collection('usuarios').doc(id).delete()

            const arrayFiltrado = usuarios.filter(item => item.id !== id)

            setUsuarios(arrayFiltrado)

            toast.success('Usuario elimado correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });


        } catch (error) {
            console.log(error)
        }
    }

    const modoEdicion = (item) => {
        setId(item.id)
        setCedula(item.cedula)
        setNombre(item.userName)
        setCorreo(item.email)
        setTelefono(item.phone)
        setRol(item.role)
        setUserId(item.uid)
        setFoto(item.photoURL)
        handleShow()
    }

    const editar = async () => {
        try {

            const db = firebase.firestore()
            await db.collection('usuarios').doc(id).update({
                cedula: cedula,
                userName: nombre,
                email: correo,
                phone: telefono,
                role: rol
            })
            const arrayEditado = usuarios.map(item => (
                item.id === id ? { id: item.id, cedula: cedula, userName: nombre, email: correo, phone: telefono, role: rol } : item
            ))

            setUsuarios(arrayEditado)
            setId('')
            handleClose()
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col">
                </div>
                <div className="col-12">
                    <Modal show={show} onHide={handleClose} className="mt-5">
                        <Modal.Header>
                            <Modal.Title>Editar Usuarios</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="container">
                                <div className="row">
                                    <div class="col">
                                        <Form>
                                            <Form.Group className="mb-3" controlId="formGridNombre">
                                                <Form.Label>Cédula</Form.Label>
                                                <Form.Control onChange={e => setCedula(e.target.value)} value={cedula} />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridCodigo">
                                                <Form.Label>Nombre</Form.Label>
                                                <Form.Control onChange={e => setNombre(e.target.value)} value={nombre} type='text' />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridPrecio">
                                                <Form.Label>Correo</Form.Label>
                                                <Form.Control type='email' onChange={e => setCorreo(e.target.value)} value={correo} />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="formGridUnidades">
                                                <Form.Label>Télefono</Form.Label>
                                                <Form.Control type='text' onChange={e => setTelefono(e.target.value)} value={telefono} />
                                            </Form.Group>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button to="Dashboard" type="button" onClick={() => editar()} className="btn btn-primary " >Guardar</button>

                            <Button variant="secondary" onClick={handleClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <div class="clearfix">
                        <div class="pull-left">
                            <div class="c-content-title-1">
                                <h3 class="font-weight-bold">Lista de Usuarios</h3>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <Link type="button" to="/CreateUser" class="btn btn-success mt-3 mb-3"><span class="fa fa-plus "></span> Registrar usuario</Link>
                        </div>
                    </div>
                    
                    <Search onSearch={(value) => {
                        setSearch(value)
                        setCurrentPage(1)
                    }
                    }
                    />
                    <table class="table table-hover table-striped tablas">
                        <div className="mb-3">
                        </div>
                        <thead>
                            <tr>
                                <th scope="col">Cédula</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Télefono</th>
                                <th scope="col">Rol</th>
                                <th scope="col">Fecha de creación</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                tableData.map((item, index) => (
                                    <tr key={index}>
                                        <th>{item.cedula}</th>
                                        <td>{item.userName}</td>
                                        <td>{item.email}</td>
                                        <td>{item.phone}</td>
                                        <td>{item.role}</td>
                                        <td>{moment(item.date).format('MMMM Do YYYY, h:mm:ss a')}</td>
                                        <td class="btn-group d-flex justify-content-center">
                                            <Button variant="warning" className="" onClick={() => modoEdicion(item)}>
                                                Editar
                                            </Button>
                                            <button type="button" class="btn btn-danger" onClick={() => eliminar(item.id)}>Eliminar</button>

                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    <Pagination
                        total={totalItems}
                        itemsPerPage={ITEMS_PER_PAGE}
                        currentPage={currentPage}
                        onPageChange={page => setCurrentPage(page)}
                    />
                    {loading && <div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>}
                </div>
                <div class="col">
                </div>
            </div>
        </div>

    )
}

export default VerUsuario