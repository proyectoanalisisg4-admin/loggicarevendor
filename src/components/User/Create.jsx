import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { addUser } from '../../redux/vendedorDucks'

import { toast, ToastContainer } from 'react-toastify';
import { Link } from 'react-router-dom'

import InputMask from 'react-input-mask';

import moment from 'moment'
import 'moment/locale/es' // Pasar a español

const Create = () => {
    const dispatch = useDispatch()

    const [cedula, setCedula] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [userName, setUserName] = React.useState('')
    const [phone, setPhone] = React.useState('')
    const [pass, setPass] = React.useState('')
    const [role, setRole] = React.useState([''])


    function ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return (false)
    }

    const addNewUser = () => {
        if (!cedula || cedula.length !== 11) {
            toast.warning('Ingrese una cédula valida', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!userName) {
            toast.warning('Ingrese un nombre', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!phone || phone.length !== 9) {
            toast.warning('Ingrese una télefono valido', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!email) {
            toast.warning('Ingrese un correo', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if (!ValidateEmail(email)) {
            toast.warning('Formato de correo incorrecto', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
        if (!pass) {
            toast.warning('Ingrese una contraseña', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
 
        if (!role === '') {
            toast.warning('Ingrese un role', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        dispatch(addUser(cedula, userName, phone, email, pass, role))

        setEmail('')
        setPhone('')
        setUserName('')
        setCedula('')
        setPass('')
        setRole([''])


    }


    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col">
                </div>
                <div className="col-10">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"> Registrar Usuario</h5>
                            <br />
                            <form>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Cédula</label>
                                    <InputMask mask="9-9999-9999" maskChar=""placeholder="1-1111-1111" type="text" class="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setCedula(e.target.value)} value={cedula} />
                                    
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Nombre</label>
                                    <InputMask type="text" class="form-control" placeholder="Nombre del usuario" id="exampleFormControlInput1" autocomplete="off" onChange={e => setUserName(e.target.value)} value={userName} />
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Télefono</label>
                                    <InputMask mask="9999-9999" maskChar="" type="text"placeholder="0000-0000" class="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setPhone(e.target.value)} value={phone} />
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Correo</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="correo@ejemplo.com" autocomplete="off" onChange={e => setEmail(e.target.value)} value={email} />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Contraseña</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña del usuario" autocomplete="off" onChange={e => setPass(e.target.value)} value={pass} />
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Rol</label>
                                    <select class="form-control" id="exampleFormControlSelect1"  autocomplete="off" onChange={e => setRole(e.target.value)} value={role}>
                                        <option> </option>
                                        <option>Administrador</option>
                                        <option>Vendedor</option>
                                    </select>
                                </div>
                                <button type="button" className="btn btn-primary mr-2" onClick={() => addNewUser()}>Guardar</button>
                                <Link type="button" className="btn btn-secondary" to="ShowUser">Cancelar</Link>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    )


}

export default Create