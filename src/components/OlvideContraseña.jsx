import React from 'react'
import { Link } from 'react-router-dom'
import { resetPassword } from '../redux/usuarioDucks'

import { withRouter } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const OlvideContraseña = (props) => {

    const dispatch = useDispatch()
    const [email, setEmail] = React.useState('')


    return (
        <div className="container mt-5">
            <ToastContainer />
            <div className="row">
                <div className="col">

                </div>
                <div className="col-6">
                    <div className="card">
                        <div className="card-header text-center">
                            <img src="/assets/images/logo.png" Style="width:300px" className="img-fluid" alt="Logo loggicare" />
                        </div>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Correo electrónico</label>
                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={e => setEmail(e.target.value)} value={email} />
                                </div>
                                <button type="button" className="btn btn-primary" onClick={() => dispatch(resetPassword(email,props))}>Enviar</button>
                                <Link to="/login" className="btn btn-primary ml-2">Volver</Link>
                                <hr></hr>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col">

                </div>
            </div>
        </div>
    )
}

export default withRouter(OlvideContraseña)
