import React from 'react'

//Redux
import { useDispatch, useSelector } from 'react-redux'
import { authWithUserAndPassword, editarFotoAccion } from '../../redux/usuarioDucks'
import { Link } from 'react-router-dom'

import InputMask from 'react-input-mask';

import { withRouter } from 'react-router-dom'

//Logica BD importar Form
import { Form } from 'react-bootstrap';
import { Modal, Button } from 'react-bootstrap';
import { auth, firebase, db, storage } from '../../firebase'


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


//table
import TableHeader from '../Datatable/Header'
import Search from '../Datatable/Search'



const IngresoVentas = (props) => {
  const dispatch = useDispatch()

  const usuario = useSelector(store => store.usuario.user) //Get loading from usuarioDucks


  //Logica para agregar a base datos de la info del formulario
  const [codigo, setCodigo] = React.useState('')
  const [nombre, setNombre] = React.useState(usuario.email)
  const [precio, setPrecio] = React.useState('')
  const [unidades, setUnidades] = React.useState('')
  //Fin BD


  const ITEMS_PER_PAGE = 8;
  const [show, setShow] = React.useState(false);
  const [loading, setLoading] = React.useState(true)

  const handleClose = () => {
    setShow(false);
  }

  const handleShow = (type) => {
    setShow(true);
  }

  const [productos, setProductos] = React.useState([])
  const [productosFactura, setProductosFactura] = React.useState([])
  const [cedula, setCedula] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [userName, setUserName] = React.useState('')
  const [phone, setPhone] = React.useState('')
  const [envio, setEnvio] = React.useState('No')
  const [direccionEnvio, setDireccionEnvio] = React.useState('')
  const [informacionAdicional, setInformacionAdicional] = React.useState('')
  const [totalItems, setTotalItems] = React.useState(0);
  const [currentPage, setCurrentPage] = React.useState();
  const [search, setSearch] = React.useState('')


  const headers = [{ name: "Código", field: "Codigo" },
  { name: "Nombre", field: "Nombre" },
  { name: "Precio", field: "Precio" },
  { name: "Cantidad", field: "Cantidad" }
  ]

  const tableData = React.useMemo(() => {
    let computedTable = productos;
    if (search) {
      computedTable = computedTable.filter(
        table => table.Nombre_Producto.toLowerCase().includes(search.toLowerCase()) ||
          table.Codigo_SKU.toLowerCase().includes(search.toLowerCase())
      )
    }
    setTotalItems(computedTable.length)

    //Current page slice
    return computedTable.slice((currentPage - 1) * ITEMS_PER_PAGE, (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE)
  }, [productos, currentPage, search])


  //Cargar datos de productos
  React.useEffect(() => {

    const obtenerDatos = async () => {
      try {
        const db = firebase.firestore()
        const data = await db.collection('registros').where("Unidad_Disponibles", ">", 0).get();
        const arrayData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }))
        setProductos(arrayData)
        setLoading(false)

      } catch (error) {
        console.log(error)
      }
    }

    obtenerDatos()
    // eslint-disable-next-line
  }, [])


  //Logica para agregar a base datos de la info del formulario
  const agregar = async (e) => {
    if (productosFactura.length === 0) {
      toast.warning('Ingrese todos los datos', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return
    }

    try {

      const db = firebase.firestore()

      let total = 0
      productosFactura.map(async (item, index) => {
        total = total + item.price

      })

      const nuevoVenta =
      {
        Vendedor: nombre,
        CedulaCliente: cedula,
        NombreCliente: userName,
        CorreoCliente: email,
        NumeroCliente: phone,
        EnvioCliente: envio,
        DireccionEnvioCliente: direccionEnvio,
        InformacionAdicionalCliente: informacionAdicional,
        Productos: productosFactura,
        Facturacion: "Pendiente",
        Total: parseInt(total),
        Fecha: Date.now()

      }

      productosFactura.map(async (item) => {
        const data = await db.collection('registros').doc(item.idProduct).get()
        if(item.value > data.data().Unidad_Disponibles){
          toast.warning('No hay inventario disponible de : ' + item.name, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          return
        }
        await db.collection('registros').doc(item.idProduct).update({
          Unidad_Disponibles: data.data().Unidad_Disponibles - item.value
        })
      }
      )

      await db.collection('ventas').add(nuevoVenta)



    }
    catch (error) {
      console.log(error)
    }

    setProductosFactura([])

    toast.success('Venta creada correctamente', {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    props.history.push("/Ventas")

  }

  const addProduct = (value, newItem) => {
    let productFound = false


    productosFactura.map(item => (
      item.id === newItem.Codigo_SKU ? productFound = true : null
    ))

    if (productFound) {
      let arrayEditado = {}
      if (value === '' || value === '0' || value === 0) {
        arrayEditado = productosFactura.filter(item => item.id !== newItem.Codigo_SKU)
      } else {
        arrayEditado = productosFactura.map((item, index) => (
          item.id === newItem.Codigo_SKU ? { id: newItem.Codigo_SKU, idProduct: newItem.id, value: value, name: newItem.Nombre_Producto, price: value * newItem.Precio } : item
        ))
      }
      if (value > newItem.Unidad_Disponibles) {
        toast.warning('Cantidad de productos disponibles: ' + newItem.Unidad_Disponibles, {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return
      }
      setProductosFactura(arrayEditado)
    } else {
      if (value > newItem.Unidad_Disponibles) {
        toast.warning('Cantidad de productos disponibles: ' + newItem.Unidad_Disponibles, {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        return
      }
      setProductosFactura([...productosFactura,
      { id: newItem.Codigo_SKU, idProduct: newItem.id, value: value, name: newItem.Nombre_Producto, price: value * newItem.Precio }
      ])

    }
  }


  return (
    <>

      <div className="container mt-5">
        <div className="row">
          <div className="col">
          </div>
          <div className="col-10">
            <Modal show={show} onHide={handleClose} className="mt-5">
              <Modal.Header>
                <Modal.Title>Productos</Modal.Title>
                <Button variant="secondary" onClick={handleClose}>
                  Cerrar
                </Button>
              </Modal.Header>
              <Modal.Body>
                <div className="table-responsive">
                  <div className="mb-3">
                    <Search onSearch={(value) => {
                      setSearch(value)
                      setCurrentPage(1)
                    }
                    }
                    />
                  </div>
                  <table className="table mt-4">
                    <TableHeader headers={headers} />
                    <tbody>
                      {
                        tableData.map((item, index) => (
                          <tr key={index}>
                            <td>{item.Codigo_SKU}</td>
                            <td>{item.Nombre_Producto}</td>
                            <td>₡{item.Precio}</td>
                            <td><input type="number" className="form-control" id="cantidadProducto" autocomplete="off" onChange={e => addProduct(e.target.value, item)} /></td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                </div>
              </Modal.Body>
            </Modal>
            <div class="card">
              <div class="card-body">
                <h5 class="card-title"> Registrar Ventas</h5>
                <br />
                <Form>
                  <div className="form-group">
                    <label htmlFor="nombreVendedor">Nombre Vendedor</label>
                    <input type="text" className="form-control" id="nombreVendedor" value={usuario.email} readOnly />
                  </div>
                  <div className="form-group">
                    <label for="exampleFormControlInput1">Cédula</label>
                    <InputMask mask="9-9999-9999" maskChar="" type="text" class="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setCedula(e.target.value)} value={cedula} />
                  </div>
                  <div className="form-group">
                    <label for="exampleFormControlInput1">Nombre</label>
                    <InputMask type="text" className="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setUserName(e.target.value)} value={userName} />
                  </div>
                  <div className="form-group">
                    <label for="exampleFormControlInput1">Télefono</label>
                    <InputMask mask="9999-9999" maskChar="" type="text" className="form-control" id="exampleFormControlInput1" autocomplete="off" onChange={e => setPhone(e.target.value)} value={phone} />
                  </div>
                  <div className="form-group">
                    <label for="exampleFormControlInput1">Correo</label>
                    <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="correo@ejemplo.com" autocomplete="off" onChange={e => setEmail(e.target.value)} value={email} />
                  </div>
                  <div className="form-group">
                    <label for="exampleFormControlSelect1">Envio</label>
                    <select className="form-control" id="exampleFormControlSelect1" autocomplete="off" onChange={e => setEnvio(e.target.value)} value={envio}>
                      <option>No</option>
                      <option>Si</option>
                    </select>
                  </div>

                  {envio === 'Si' ?
                    <>
                      <div className="form-group">
                        <label for="exampleFormControlSelect1">Dirección envio</label>
                        <textarea type="text" className="form-control" id="dirrecionEnvioInput" autocomplete="off" onChange={e => setDireccionEnvio(e.target.value)} value={direccionEnvio} />
                      </div>
                      <div className="form-group">
                        <label for="exampleFormControlSelect1">Información adicional</label>
                        <textarea type="text" className="form-control" id="informacionAdicionalEnvioInput" autocomplete="off" onChange={e => setInformacionAdicional(e.target.value)} value={informacionAdicional} />
                      </div>
                    </>
                    : null}
                  <div className="form-group">
                    <label htmlFor="fromValue">Productos</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" onClick={() => handleShow('From')} type="button" id="button-addon1"><i class="fas fa-search-plus"></i></button>
                      </div>
                      <input type="text" autoComplete="off" className="form-control" id="fromValue" readOnly />
                    </div>
                  </div>
                  <table className="table mt-4">
                    <TableHeader headers={headers} />
                    <tbody>
                      {
                        productosFactura.map((item, index) => (
                          <tr key={index}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.value}</td>
                            <td>₡ {item.price}</td>
                          </tr>
                        ))
                      }
                    </tbody>
                  </table>
                  <button to="Dashboard" type="button" className="btn btn-primary mr-3" onClick={() => agregar()}>Guardar</button>
                  <Link to="/Ventas" className="btn btn-secondary">Cancelar</Link>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

export default withRouter(IngresoVentas)

