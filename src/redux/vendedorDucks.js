import { auth, firebase, db, storage } from '../firebase'
import { toast } from 'react-toastify';


//data inicial
const dataInicial = {
    loading: false,
}

const LOADING = 'LOADING'
const FINISH_LOADING = 'FINISH_LOADING'
const VENDEDOR_ERROR = 'VENDEDOR_ERROR'
const VENDEDOR_EXITO = 'VENDEDOR_EXITO'


//reducer
export default function usuarioReducer(state = dataInicial, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true }
        case FINISH_LOADING:
            return { ...state, loading: false }
        case VENDEDOR_ERROR:
            return { ...dataInicial }
        case VENDEDOR_EXITO:
            return { ...state, loading: false, user: action.payload, userAuth: true, activo: true }
        default:
            return { ...state }
    }
}

export const addUser = (cedula, user, phone, email, pass, role) => async (dispatch) => {
    try {

        let cedulaFound = false

        const data = await db.collection('usuarios').get()

        data.docs.map((doc) => {
            console.log(doc.data().cedula)
            if (doc.data().cedula === cedula) {
                cedulaFound = true
            }

        })

        if (!cedulaFound) {
            const res = await auth.createUserWithEmailAndPassword(email, pass)
            const usuario = {
                uid: res.user.uid,
                email: res.user.email,
                displayName: res.user.displayName,
                photoURL: res.user.photoURL,
                role: role,
                cedula: cedula,
                userName: user,
                phone: phone,
                date: Date.now()
            }
            await db.collection('usuarios').doc(res.user.email).set(usuario)
            toast.info('Usuario agregado correctamente', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            toast.info('La cédula ya existe', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }

    } catch (error) {
        console.log(error)
        if (error.code === 'auth/invalid-email') {
            toast.warning('Correo invalido', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        if (error.code === 'auth/email-already-in-use') {
            toast.warning('El correo ya esta en uso', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        if (error.code === 'auth/weak-password') {
            toast.warning('La contraseña no cumple con los parametros', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }
}