import { auth, firebase, db, storage } from '../firebase'
import { toast } from 'react-toastify';


//data inicial
const dataInicial = {
    loading: false,
    activo: false,
    sendResetPassword: false,
    userAuth: false,
}

//types
const LOADING = 'LOADING'
const FINISH_LOADING = 'FINISH_LOADING'
const USUARIO_ERROR = 'USUARIO_ERROR'
const USUARIO_EXITO = 'USUARIO_EXITO'
const FOTO_EXITO = 'FOTO_EXITO'
const USUARIO_LOGOUT = 'USUARIO_LOGOUT'
const RESET_PASSWORD_EXITO = 'RESET_PASSWORD_EXITO'


//reducer
export default function usuarioReducer(state = dataInicial, action) {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true }
        case FINISH_LOADING:
            return { ...state, loading: false }
        case USUARIO_ERROR:
            return { ...dataInicial }
        case USUARIO_EXITO:
            return { ...state, loading: false, user: action.payload, userAuth: true, activo: true }
        case FOTO_EXITO:
            return { ...state, loading: false, foto: action.payload }
        case USUARIO_LOGOUT:
            return { ...dataInicial }
        case RESET_PASSWORD_EXITO:
            return { ...state, sendResetPassword: true }
        default:
            return { ...state }
    }
}

//action
export const authWithGoogle = () => async (dispatch) => {
    dispatch({
        type: LOADING
    })
    try {
        const provider = new firebase.auth.GoogleAuthProvider();
        const res = await auth.signInWithPopup(provider)

        console.log(res.user)

        const usuario = {
            uid: res.user.uid,
            email: res.user.email,
            displayName: res.user.displayName,
            photoURL: res.user.photoURL
        }

        const usuarioDB = await db.collection('usuarios').doc(usuario.email).get()
        if (usuarioDB.exists) {
            //cuando exista el usuario
            dispatch({
                type: USUARIO_EXITO,
                payload: usuarioDB.data()
            })
            localStorage.setItem('usuario', JSON.stringify(usuarioDB.data()))

        } else {
            //No existe el usuario en firestore
            await db.collection('usuarios').doc(usuario.email).set(usuario)

            dispatch({
                type: USUARIO_EXITO,
                payload: usuario
            })
            localStorage.setItem('usuario', JSON.stringify(usuario))
        }

    } catch (error) {
        console.log(error)
        dispatch({
            type: USUARIO_ERROR
        })
    }
}

export const authWithUserAndPassword = (email, pass) => async (dispatch) => {
    dispatch({
        type: LOADING
    })
    try {
        const res = await auth.signInWithEmailAndPassword(email, pass)

        const usuarioDB = await db.collection('usuarios').doc(res.user.email).get()

        if (usuarioDB.exists) {
            //cuando exista el usuario
            dispatch({
                type: USUARIO_EXITO,
                payload: usuarioDB.data()
            })
            localStorage.setItem('usuario', JSON.stringify(usuarioDB.data()))

        } else {
            toast.info('El usuario no fue encontrado', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    } catch (error) {
        console.log(error)
        if (error.code === 'auth/user-not-found') {
            toast.info('El usuario no fue encontrado', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        if (error.code === 'auth/wrong-password') {
            toast.info('El usuario no fue encontrado', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }
    dispatch({
        type: FINISH_LOADING
    })
}

export const resetPassword = (email, props) => async (dispatch) => {
    try {
        auth.languageCode = 'es'; 
        await auth.sendPasswordResetEmail(email)
        dispatch({
            type: RESET_PASSWORD_EXITO
        })
        toast.info('Por favor revise su correo y siga las instrucciones para cambiar la contraseña', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

    } catch (error) {
        toast.info('El correo suministrado no existe', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
}

export const leerUsuarioActivoAccion = () => (dispatch) => {
    if (localStorage.getItem('usuario')) {
        dispatch({
            type: USUARIO_EXITO,
            payload: JSON.parse(localStorage.getItem('usuario'))
        })
    }
}

export const cerrarSesionAccion = () => (dispatch) => {
    auth.signOut()
    localStorage.removeItem('usuario')
    dispatch({
        type: USUARIO_LOGOUT
    })
}

export const actualizarUsuarioAccion = (nombreActualizado) => async (dispatch, getState) => {
    dispatch({
        type: LOADING
    })

    const { user } = getState().usuario
    console.log(user)

    try {

        await db.collection('usuarios').doc(user.email).update({
            displayName: nombreActualizado
        })

        const usuario = {
            ...user,
            displayName: nombreActualizado
        }

        dispatch({
            type: USUARIO_EXITO,
            payload: usuario
        })

        localStorage.setItem('usuario', JSON.stringify(usuario))

    } catch (error) {
        console.log(error)
    }
}

export const editarFotoAccion = (imagenEditada, codigo, id) => async (dispatch, getState) => {
    dispatch({
        type: LOADING
    })

    const { user } = getState().usuario

    try {
        const imagenRef = storage.ref().child(codigo).child('portada')
        await imagenRef.put(imagenEditada)
        const imagenURL = await imagenRef.getDownloadURL()
        await db.collection('registros').doc(id).update({
            photoURL: imagenURL
        })

        const foto = {
            photoURL: imagenURL
        }

        await dispatch({
            type: FOTO_EXITO,
            payload: foto
        })


    } catch (error) {
        console.log(error)
        dispatch({
            type: FINISH_LOADING
        })
    }

}

export const agregarEnvioAccion = (id) => async (dispatch, getState) => {
    dispatch({
        type: LOADING
    })

    const { user } = getState().usuario

    try {
        await db.collection('registros').doc(id).update({
           
        })

        const usuario = {
            ...user,
        }

        dispatch({
            type: USUARIO_EXITO,
            payload: usuario
        })


    } catch (error) {
        console.log(error)
        dispatch({
            type: FINISH_LOADING
        })
    }


}

export const editarFotoPerfil = (imagenEditada) => async (dispatch, getState) => {
    dispatch({
        type: LOADING
    })

    const { user } = getState().usuario

    try {
        const imagenRef = await storage.ref().child(user.email).child('Foto Perfil')
        await imagenRef.put(imagenEditada)
        const imagenURL = await imagenRef.getDownloadURL()
        await db.collection('usuarios').doc(user.email).update({
            photoURL: imagenURL
        })

        const usuario = {
            ...user,
            photoURL: imagenURL
        }

        await dispatch({
            type: USUARIO_EXITO,
            payload: usuario
        })


    } catch (error) {
        console.log(error)
        dispatch({
            type: FINISH_LOADING
        })
    }

}