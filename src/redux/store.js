import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import vendedorReducer from './vendedorDucks'
import usuarioReducer,{leerUsuarioActivoAccion} from './usuarioDucks'


const rootReducer = combineReducers({
    usuario: usuarioReducer,
    vendedor: vendedorReducer
})


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    const store = createStore(rootReducer,composeEnhancers(applyMiddleware(thunk)))
    leerUsuarioActivoAccion()(store.dispatch) //leer usuario al principio
    return store;
}