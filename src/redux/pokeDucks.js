import axios from 'axios'


//constantes
const dataInicial = {
    count: 0,
    next: null,
    previous: null,
    results: []
}

const GET_POKEMON_SUCCESS = 'GET_POKEMON_SUCCESS'
const NEXT_POKEMON_SUCCESS = 'NEXT_POKEMON_SUCCESS'
const POKE_INFO_EXITO = 'POKE_INFO_EXITO'



//reducer
export default function pokeReducer(state = dataInicial, action) {
    switch (action.type) {
        case GET_POKEMON_SUCCESS:
            return {...state, ...action.payload }
        case NEXT_POKEMON_SUCCESS:
            return {...state, ...action.payload }
        case POKE_INFO_EXITO:
            return {...state, unPokemon: action.payload }
        default:
            return state
    }
}

//acciones

export const unPokeDetalleAccion = (url = 'https://pokeapi.co/api/v2/pokemon/1/') => async(dispatch) => {

    if (localStorage.getItem(url)) {
        dispatch({
            type: POKE_INFO_EXITO,
            payload: JSON.parse(localStorage.getItem(url))
        })
        console.log("desde localstorage")
        return
    }
    try {
        console.log("desde api")
        const res = await axios.get(url)
        dispatch({
            type: POKE_INFO_EXITO,
            payload: {
                nombre: res.data.name,
                ancho: res.data.weight,
                alto: res.data.height,
                foto: res.data.sprites.front_default
            }
        })
        localStorage.setItem(url, JSON.stringify({
            nombre: res.data.name,
            ancho: res.data.weight,
            alto: res.data.height,
            foto: res.data.sprites.front_default
        }))
    } catch (error) {
        console.log(error)
    }
}


export const getPokemonAction = () => async(dispatch) => {

    //console.log('Get state:', getState().pokemones.offset)
    //const offset = getState().pokemones.offset
    if (localStorage.getItem('offset=0')) {
        console.log('datos guardados')
        dispatch({
            type: GET_POKEMON_SUCCESS,
            payload: JSON.parse(localStorage.getItem('offset=0')) //String to json
        })
        return
    }
    try {
        console.log('datos API')
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=0&limit=10`)
        dispatch({
            type: GET_POKEMON_SUCCESS,
            payload: res.data
        })
        localStorage.setItem('offset=0', JSON.stringify(res.data)) //Json to string
    } catch (error) {
        console.log('getPokemonAction: ' + error)
    }
}


export const nextPokemonAction = () => async(dispatch, getState) => {
    //Primera alternativa
    //const offset = getState().pokemones.offset
    //const siguiente = offset + nextLimit
    const next = getState().pokemones.next

    console.log('Next: ', next)
    if (localStorage.getItem(next)) {
        dispatch({
            type: NEXT_POKEMON_SUCCESS,
            payload: JSON.parse(localStorage.getItem(next))
        })
        return
    }
    try {
        const res = await axios.get(next)
        dispatch({
            type: NEXT_POKEMON_SUCCESS,
            payload: res.data
        })
        localStorage.setItem(next, JSON.stringify(res.data))
    } catch (error) {
        console.log(error)
    }
}


export const previousPokemonAction = () => async(dispatch, getState) => {

    const { previous } = getState().pokemones
    if (localStorage.getItem(previous)) {
        dispatch({
            type: NEXT_POKEMON_SUCCESS,
            payload: JSON.parse(localStorage.getItem(previous))
        })
        return
    }
    try {
        const res = await axios.get(previous)
        dispatch({
            type: NEXT_POKEMON_SUCCESS,
            payload: res.data
        })
    } catch (error) {
        console.log(error)
    }
}