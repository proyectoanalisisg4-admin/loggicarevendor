import React from 'react'
import Login from "./components/Login";
import OlvideContraseña from "./components/OlvideContraseña";
import Perfil from './components/Perfil';
import Container from './components/Container';
import Vista_productos from './components/Inventario/Inventario';
import Inventario from './components/Inventario/Inventario';
import Envios from './components/Envios/Envios';
import Ventas from './components/Ventas/Ventas';
import IngresoVentas from './components/Ventas/IngresoVentas';
import CreateUser from './components/User/Create';
import ShowUser from './components/User/VerUsuario';
import Factura from './components/Factura/Create';
import VerFactura from './components/Factura/Index';
import Facturar from './components/Factura/Facturarventas';
import GraficosAdm from './components/Graficos/Graficosadm';
import GraficosVen from './components/Graficos/Graficosvendedor';
import { auth } from './firebase'
import { ThemeProvider } from '@mui/material/styles';
import theme from './temaConfig'


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Formulario_Ingreso from './components/Inventario/Formulario_ingreso';
import IngresoEnvios from './components/Envios/IngresoEnvios';


function App() {

  const [firebaseUser, setFirebaseUser] = React.useState(false)


  React.useEffect(() => {

    const fetchUser = () => {

      auth.onAuthStateChanged(user => {
        if (localStorage.getItem('usuario')) {
          setFirebaseUser(JSON.parse(localStorage.getItem('usuario')))
        } else {
          if (user) {
            setFirebaseUser(user)
          } else {
            setFirebaseUser(null)
          }
        }

      })


    }

    fetchUser()



  }, [])

  const RutaPrivada = ({ component, path, ...rest }) => {
    if (localStorage.getItem('usuario')) {
      const usuarioStorage = JSON.parse(localStorage.getItem('usuario'))
      if (usuarioStorage.uid === firebaseUser.uid) {
        return <Route component={component} path={path} {...rest} />
      } else {
        return <Redirect to="/login" {...rest} />
      }
    } else {
      return <Redirect to="/login" {...rest} />
    }
  }

  return firebaseUser !== false ? (
    <ThemeProvider theme={theme}>
      <Router>
          <Container />
        <Switch>
          <Route path="/login" exact>
            <Login />
          </Route>
          <Route path="/OlvideContraseña" exact>
            <OlvideContraseña />
          </Route>
          <RutaPrivada path="/Inventario" exact>
            <Inventario />
          </RutaPrivada>
          <RutaPrivada path="/AñadirProducto" exact>
            <Formulario_Ingreso />
          </RutaPrivada>
          <RutaPrivada path="/Perfil" exact>
            <Perfil />
          </RutaPrivada>
          <RutaPrivada path="/Envios" exact>
            <Envios />
          </RutaPrivada>
          <RutaPrivada path="/IngresoEnvios" exact>
            <IngresoEnvios />
          </RutaPrivada>
          <RutaPrivada path="/CreateUser" exact>
            <CreateUser />
          </RutaPrivada>
          <RutaPrivada path="/ShowUser" exact>
            <ShowUser />
          </RutaPrivada>
          <RutaPrivada path="/CrearFactura" exact>
            <Factura/>
          </RutaPrivada>
          <RutaPrivada path="/VerFacturas" exact>
            <VerFactura/>
          </RutaPrivada>
          <RutaPrivada path="/Facturar" exact>
            <Facturar/>
          </RutaPrivada>
          <RutaPrivada path="/Ventas" exact>
            <Ventas />
          </RutaPrivada>
          <RutaPrivada path="/IngresoVentas" exact>
            <IngresoVentas />
          </RutaPrivada>
          <RutaPrivada path="/Graficos" exact>
            <GraficosAdm />
          </RutaPrivada>
          <RutaPrivada path="/GraficosVendedor" exact>
            <GraficosVen />
          </RutaPrivada>
          
        </Switch>
      </Router>
    </ThemeProvider>
  ) : (<div className="container mt-5">
    <div className="row">
      <div className="col">
      </div>
      <div className="col-10">
        <div class="d-flex justify-content-center">
          <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      </div>
      <div class="col">
      </div>
    </div>
  </div>)
}

export default App;
